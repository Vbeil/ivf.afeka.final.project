package ivf.data;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ivf.dataDeserializers.ExcelDateTimeDeserializer;
import ivf.dataDeserializers.IntegerDeserializer;
import ivf.dataDeserializers.StringNullCleanerDeserializer;

@Entity
@Table(name = "PuStorage")
public class PuStorageEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long rowId;

	@JsonProperty("idHash")
	private String patientId;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	@JsonProperty("admissionNoHash")
	private String admission;

	@JsonDeserialize(using = ExcelDateTimeDeserializer.class)
	private LocalDateTime freezeDate;

	@JsonDeserialize(using = ExcelDateTimeDeserializer.class)
	private LocalDateTime defrostDate;

	@JsonDeserialize(using = IntegerDeserializer.class)
	@JsonProperty("rowId")
	private Integer puRowId;

	@JsonDeserialize(using = IntegerDeserializer.class)
	@JsonProperty("tubeRowId")
	private Integer puTubeRowId;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String ovumType;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer dayNo;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer ovumCount;

	public PuStorageEntity() {

	}

	public PuStorageEntity(String patientId, String admission, LocalDateTime freezeDate, LocalDateTime defrostDate,
			Integer puRowId, Integer puTubeRowId, String ovumType, Integer dayNo, Integer ovumCount) {
		super();
		this.patientId = patientId;
		this.admission = admission;
		this.freezeDate = freezeDate;
		this.defrostDate = defrostDate;
		this.puRowId = puRowId;
		this.puTubeRowId = puTubeRowId;
		this.ovumType = ovumType;
		this.dayNo = dayNo;
		this.ovumCount = ovumCount;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public String getAdmission() {
		return admission;
	}

	public void setAdmission(String admission) {
		this.admission = admission;
	}

	public LocalDateTime getFreezeDate() {
		return freezeDate;
	}

	public void setFreezeDate(LocalDateTime freezeDate) {
		this.freezeDate = freezeDate;
	}

	public LocalDateTime getDefrostDate() {
		return defrostDate;
	}

	public void setDefrostDate(LocalDateTime defrostDate) {
		this.defrostDate = defrostDate;
	}

	public Integer getPuRowId() {
		return puRowId;
	}

	public void setPuRowId(Integer puRowId) {
		this.puRowId = puRowId;
	}

	public Integer getPuTubeRowId() {
		return puTubeRowId;
	}

	public void setPuTubeRowId(Integer puTubeRowId) {
		this.puTubeRowId = puTubeRowId;
	}

	public String getOvumType() {
		return ovumType;
	}

	public void setOvumType(String ovumType) {
		this.ovumType = ovumType;
	}

	public Integer getDayNo() {
		return dayNo;
	}

	public void setDayNo(Integer dayNo) {
		this.dayNo = dayNo;
	}

	public Integer getOvumCount() {
		return ovumCount;
	}

	public void setOvumCount(Integer ovumCount) {
		this.ovumCount = ovumCount;
	}

}
