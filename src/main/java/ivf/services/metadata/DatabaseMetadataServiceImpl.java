package ivf.services.metadata;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.JDBCType;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;

import ivf.boundaries.TableColumnBoundary;
import ivf.util.ConsoleLogger;

@Service
public class DatabaseMetadataServiceImpl implements DatabaseMetadataService {

	/**
	 * Gets the database connection URL from the configuration
	 */
	@Value("${spring.datasource.url}")
	private String dbConnectionURL;

	/**
	 * Gets the database connection User name from the configuration
	 */
	@Value("${spring.datasource.username}")
	private String dbUserName;

	/**
	 * Gets the database connection Password from the configuration
	 */
	@Value("${spring.datasource.password}")
	private String dbUserPass;

	private ConsoleLogger logger;

	@PostConstruct
	public void init() {
		this.logger = new ConsoleLogger("Database Metadata");
	}

	@Override
	public String[] getDatabaseTables() throws SQLException {
		logger.Print("Retrieving the database tables", false);
		ArrayList<String> tableNames = new ArrayList<>();

		try (Connection con = DriverManager.getConnection(dbConnectionURL, dbUserName, dbUserPass)) {
			DatabaseMetaData dbmd = con.getMetaData();
			ResultSet tablesResultSet = dbmd.getTables(null, null, null, new String[] { "TABLE" });
			while (tablesResultSet.next()) {
				tableNames.add(tablesResultSet.getString("TABLE_NAME"));
			}

		}
		logger.Print("Finished retrieving database tables", false);
		return tableNames.toArray(new String[0]);
	}


	@Override
	public TableColumnBoundary[] getTableColumnsAndDataType(String tableName)
			throws SQLException, JsonProcessingException {
		logger.Print("Retrieving the columns and datatypes of the following table: " + tableName, false);
		ArrayList<TableColumnBoundary> columns = new ArrayList<>();

		try (Connection con = DriverManager.getConnection(dbConnectionURL, dbUserName, dbUserPass)) {
			DatabaseMetaData dbmd = con.getMetaData();
			ResultSet columnsResultSet = dbmd.getColumns(null, null, tableName, null);
			while (columnsResultSet.next()) {
				String columnName = columnsResultSet.getString("COLUMN_NAME");
				int columnDataTypeCode = Integer.valueOf(columnsResultSet.getString("DATA_TYPE"));
				String columnDataTypeName = JDBCType.valueOf(columnDataTypeCode).getName();
				columns.add(new TableColumnBoundary(columnName, columnDataTypeName));
			}
		}

		logger.Print("Finished retrieving the columns and datatypes of the following table:" + tableName, false);
		return columns.toArray(new TableColumnBoundary[0]);
	}

}
