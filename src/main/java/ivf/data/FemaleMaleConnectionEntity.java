package ivf.data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ivf.dataDeserializers.StringNullCleanerDeserializer;

@Entity
@Table(name = "FemaleMaleConnection")
public class FemaleMaleConnectionEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long rowId;

	@JsonProperty("idHashFemale")
	private String patientId;

	@JsonProperty("genderF")
	private String genderFemale;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	@JsonProperty("idHashMal")
	private String patientPartnerId;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	@JsonProperty("genderM")
	private String genderMale;

	public FemaleMaleConnectionEntity() {
	}

	public FemaleMaleConnectionEntity(String patientId, String genderFemale, String patientPartnerId,
			String genderMale) {
		super();
		this.patientId = patientId;
		this.genderFemale = genderFemale;
		this.patientPartnerId = patientPartnerId;
		this.genderMale = genderMale;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public String getGenderFemale() {
		return genderFemale;
	}

	public void setGenderFemale(String genderFemale) {
		this.genderFemale = genderFemale;
	}

	public String getPatientPartnerId() {
		return patientPartnerId;
	}

	public void setPatientPartnerId(String patientPartnerId) {
		this.patientPartnerId = patientPartnerId;
	}

	public String getGenderMale() {
		return genderMale;
	}

	public void setGenderMale(String genderMale) {
		this.genderMale = genderMale;
	}

}
