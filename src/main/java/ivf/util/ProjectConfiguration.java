package ivf.util;

public final class ProjectConfiguration {

	// Query Properties
	public static final Integer MAX_RESULT_SET_SIZE = 50000;

	// Cache configuration
	// public static final Long CACHE_MAX_SIZE = 100L;
	public static final Integer CACHE_MAX_LIFETIME_HOURS = 24;
	public static final Long CACHE_MAX_WEIGHT = 1500000L;

	// URL's
	public static final String URL_MAIN = "/ivf";

	// Research URL's
	private static final String RESEARCH = "/research";
	private static final String RESEARCH_QUERY = "/query";
	private static final String RESEARCH_QUERY_JSON = "/query/json";

	public static final String URL_RESEARCH_QUERY = URL_MAIN + RESEARCH + RESEARCH_QUERY;
	public static final String URL_RESEARCH_QUERY_JSON = URL_MAIN + RESEARCH + RESEARCH_QUERY_JSON;

	// Tables MetaData URL's
	private static final String TABLES = "/tables";
	private static final String SPECIFIC_TABLE = "/{tableName}";

	public static final String URL_TABLES = URL_MAIN + TABLES;
	public static final String URL_TABLE_COLUMNS = URL_MAIN + TABLES + SPECIFIC_TABLE;

	// Data Upload URL's
	private static final String PATIENTS = "/visits";
	private static final String IVF_HISTORY = "/ivfhistory";
	private static final String OTHER_TESTS = "/othertests";
	private static final String DIAGNOSIS = "/ivfdiagnosis";
	private static final String PROTOCOLS = "/protocols";
	private static final String PROTOCOLS_FOLLOWUP = "/protocolsfollowup";
	private static final String PROTOCOLS_PROCEDURES = "/protocolsprocedurs";
	private static final String PROTOCOLS_US = "/protocolsus";
	private static final String PROTOCOLS_DRUGS = "/protocolsdrugs";
	private static final String OPU = "/opu";
	private static final String OPU_DRUGS = "/opudrugs";
	private static final String FEMALE_MALE_CONNECTION = "/femalemaleconnection";
	private static final String RETURN_ACTION = "/returnaction";
	private static final String RETURN_ACTION_DRUGS = "/returnactiondrugs";
	private static final String SPERM = "/spearm";
	private static final String OVUM_FOLLOWUP = "/ovumfollowup";
	private static final String PU_STORAGE = "/pustorage";
	private static final String IVF_TREATMENT_SUMMARY = "/ivftreatmentsummary";
	private static final String US_PREGNANCY = "/uspregnancy";
	private static final String MAN_IVF = "/manivf";
	private static final String COMORBIDITS = "/comorbidits";
	private static final String HABITS = "/habits";
	private static final String BMI = "/bmi";
	private static final String ACCESS = "/ivfoldsys";

	private static final String OVERRIDE = "/{override}";
	private static final String DATABASE_CLEAR = "/clearDatabase";

	public static final String URL_DATA_UPLOAD = URL_MAIN + "/dataUpload";
	public static final String URL_DATA_UPLOAD_PATIENTS = URL_DATA_UPLOAD + PATIENTS + OVERRIDE;
	public static final String URL_DATA_UPLOAD_IVF_HISTORY = URL_DATA_UPLOAD + IVF_HISTORY + OVERRIDE;
	public static final String URL_DATA_UPLOAD_OTHER_TESTS = URL_DATA_UPLOAD + OTHER_TESTS + OVERRIDE;
	public static final String URL_DATA_UPLOAD_DIAGNOSIS = URL_DATA_UPLOAD + DIAGNOSIS + OVERRIDE;
	public static final String URL_DATA_UPLOAD_PROTOCOLS = URL_DATA_UPLOAD + PROTOCOLS + OVERRIDE;
	public static final String URL_DATA_UPLOAD_PROTOCOLS_FOLLOWUP = URL_DATA_UPLOAD + PROTOCOLS_FOLLOWUP + OVERRIDE;
	public static final String URL_DATA_UPLOAD_PROTOCOLS_PROCEDURES = URL_DATA_UPLOAD + PROTOCOLS_PROCEDURES + OVERRIDE;
	public static final String URL_DATA_UPLOAD_PROTOCOLS_US = URL_DATA_UPLOAD + PROTOCOLS_US + OVERRIDE;
	public static final String URL_DATA_UPLOAD_PROTOCOLS_DRUGS = URL_DATA_UPLOAD + PROTOCOLS_DRUGS + OVERRIDE;
	public static final String URL_DATA_UPLOAD_OPU = URL_DATA_UPLOAD + OPU + OVERRIDE;
	public static final String URL_DATA_UPLOAD_OPU_DRUGS = URL_DATA_UPLOAD + OPU_DRUGS + OVERRIDE;
	public static final String URL_DATA_UPLOAD_FEMALE_MALE_CONNECTION = URL_DATA_UPLOAD + FEMALE_MALE_CONNECTION
			+ OVERRIDE;
	public static final String URL_DATA_UPLOAD_RETURN_ACTION = URL_DATA_UPLOAD + RETURN_ACTION + OVERRIDE;
	public static final String URL_DATA_UPLOAD_RETURN_ACTION_DRUGS = URL_DATA_UPLOAD + RETURN_ACTION_DRUGS + OVERRIDE;
	public static final String URL_DATA_UPLOAD_SPERM = URL_DATA_UPLOAD + SPERM + OVERRIDE;
	public static final String URL_DATA_UPLOAD_OVUM_FOLLOWUP = URL_DATA_UPLOAD + OVUM_FOLLOWUP + OVERRIDE;
	public static final String URL_DATA_UPLOAD_PU_STORAGE = URL_DATA_UPLOAD + PU_STORAGE + OVERRIDE;
	public static final String URL_DATA_UPLOAD_IVF_TREATMENT_SUMMARY = URL_DATA_UPLOAD + IVF_TREATMENT_SUMMARY
			+ OVERRIDE;
	public static final String URL_DATA_UPLOAD_US_PREGNANCY = URL_DATA_UPLOAD + US_PREGNANCY + OVERRIDE;
	public static final String URL_DATA_UPLOAD_MAN_IVF = URL_DATA_UPLOAD + MAN_IVF + OVERRIDE;
	public static final String URL_DATA_UPLOAD_COMORBIDITS = URL_DATA_UPLOAD + COMORBIDITS + OVERRIDE;
	public static final String URL_DATA_UPLOAD_HABITS = URL_DATA_UPLOAD + HABITS + OVERRIDE;
	public static final String URL_DATA_UPLOAD_BMI = URL_DATA_UPLOAD + BMI + OVERRIDE;
	public static final String URL_DATA_UPLOAD_ACCESS = URL_DATA_UPLOAD + ACCESS + OVERRIDE;

	public static final String URL_DATABASE_CLEAR = URL_DATA_UPLOAD + DATABASE_CLEAR;

	// Logger
	public static final String LINE_SEPARATOR = "*******************************************************************************";

}
