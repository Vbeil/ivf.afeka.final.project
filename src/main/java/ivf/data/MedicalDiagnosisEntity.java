//package ivf.data;
//
//import java.time.LocalDate;
//
//import javax.persistence.Entity;
//import javax.persistence.FetchType;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.ManyToOne;
//import javax.persistence.Table;
//
//@Entity
//@Table(name = "MedicalDiagnosis")
//public class MedicalDiagnosisEntity {
//
//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	private Long Id;
//
//	// Foreign Key - Patients Table
//	@ManyToOne(fetch = FetchType.LAZY)
//	private PatientEntity patient;
//
//	private LocalDate recordDate;
//	private String diagnosisCode;
//	private String diagnosisDescription;
//
//	public MedicalDiagnosisEntity() {
//	}
//
//	public MedicalDiagnosisEntity(Long id, PatientEntity patient, LocalDate recordDate, String diagnosisCode,
//			String diagnosisDescription) {
//		super();
//		Id = id;
//		this.patient = patient;
//		this.recordDate = recordDate;
//		this.diagnosisCode = diagnosisCode;
//		this.diagnosisDescription = diagnosisDescription;
//	}
//
//	public Long getId() {
//		return Id;
//	}
//
//	public void setId(Long id) {
//		Id = id;
//	}
//
//	public PatientEntity getPatient() {
//		return patient;
//	}
//
//	public void setPatient(PatientEntity patient) {
//		this.patient = patient;
//	}
//
//	public LocalDate getRecordDate() {
//		return recordDate;
//	}
//
//	public void setRecordDate(LocalDate recordDate) {
//		this.recordDate = recordDate;
//	}
//
//	public String getDiagnosisCode() {
//		return diagnosisCode;
//	}
//
//	public void setDiagnosisCode(String diagnosisCode) {
//		this.diagnosisCode = diagnosisCode;
//	}
//
//	public String getDiagnosisDescription() {
//		return diagnosisDescription;
//	}
//
//	public void setDiagnosisDescription(String diagnosisDescription) {
//		this.diagnosisDescription = diagnosisDescription;
//	}
//
//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = 1;
//		result = prime * result + ((Id == null) ? 0 : Id.hashCode());
//		result = prime * result + ((diagnosisCode == null) ? 0 : diagnosisCode.hashCode());
//		result = prime * result + ((diagnosisDescription == null) ? 0 : diagnosisDescription.hashCode());
//		result = prime * result + ((patient == null) ? 0 : patient.hashCode());
//		result = prime * result + ((recordDate == null) ? 0 : recordDate.hashCode());
//		return result;
//	}
//
//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj)
//			return true;
//		if (obj == null)
//			return false;
//		if (getClass() != obj.getClass())
//			return false;
//		MedicalDiagnosisEntity other = (MedicalDiagnosisEntity) obj;
//		if (Id == null) {
//			if (other.Id != null)
//				return false;
//		} else if (!Id.equals(other.Id))
//			return false;
//		if (diagnosisCode == null) {
//			if (other.diagnosisCode != null)
//				return false;
//		} else if (!diagnosisCode.equals(other.diagnosisCode))
//			return false;
//		if (diagnosisDescription == null) {
//			if (other.diagnosisDescription != null)
//				return false;
//		} else if (!diagnosisDescription.equals(other.diagnosisDescription))
//			return false;
//		if (patient == null) {
//			if (other.patient != null)
//				return false;
//		} else if (!patient.equals(other.patient))
//			return false;
//		if (recordDate == null) {
//			if (other.recordDate != null)
//				return false;
//		} else if (!recordDate.equals(other.recordDate))
//			return false;
//		return true;
//	}
//
//}
