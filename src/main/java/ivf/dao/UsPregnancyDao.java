package ivf.dao;

import org.springframework.data.repository.CrudRepository;

import ivf.data.UsPregnancyEntity;

public interface UsPregnancyDao extends CrudRepository<UsPregnancyEntity, Long> {

}
