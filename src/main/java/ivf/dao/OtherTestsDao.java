package ivf.dao;

import org.springframework.data.repository.CrudRepository;

import ivf.data.OtherTestsEntity;

public interface OtherTestsDao extends CrudRepository<OtherTestsEntity, Long> {

}
