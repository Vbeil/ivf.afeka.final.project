package ivf.dao;

import org.springframework.data.repository.CrudRepository;

import ivf.data.ReturnActionEntity;

public interface ReturnActionDao extends CrudRepository<ReturnActionEntity, Long> {

}
