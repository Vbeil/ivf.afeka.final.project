package ivf.dataDeserializers;

import java.io.IOException;
import java.time.LocalTime;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import ivf.dataDeserializers.helpers.SerialDate;

public class ExcelTimeDeserializer extends JsonDeserializer<LocalTime> {

	@Override
	public LocalTime deserialize(JsonParser parser, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {

		String timeStr = parser.getText();

		if (timeStr.isEmpty() || timeStr == null || timeStr.equalsIgnoreCase("NULL")) {
			return null;
		}

		SerialDate sd = new SerialDate(Double.parseDouble(timeStr));
		LocalTime time = LocalTime.ofSecondOfDay(sd.toDaySeconds());

		return time;
	}

}
