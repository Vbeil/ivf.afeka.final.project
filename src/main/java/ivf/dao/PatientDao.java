package ivf.dao;

import org.springframework.data.repository.CrudRepository;

import ivf.data.PatientEntity;

public interface PatientDao extends CrudRepository<PatientEntity, Long> {

}
