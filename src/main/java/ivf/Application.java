package ivf;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import ivf.util.ConsoleLogger;

@SpringBootApplication
public class Application {

	public static String APPLICATION_NAME;
	public static String DELIMITER = "%";
	private ConsoleLogger logger;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Value("${spring.application.name:beilinson}")
	public void setHelperName(String applicationName) {
		APPLICATION_NAME = applicationName;
	}
	
	@PostConstruct
	public void init() {
		this.logger = new ConsoleLogger("Main Application");
	}

	@PostConstruct
	public void afterInit() {
		logger.Print(APPLICATION_NAME, true);
	}

}
