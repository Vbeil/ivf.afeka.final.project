package ivf.dao;

import org.springframework.data.repository.CrudRepository;

import ivf.data.DiagnosisEntity;

public interface DiagnosisDao extends CrudRepository<DiagnosisEntity, Long> {

}
