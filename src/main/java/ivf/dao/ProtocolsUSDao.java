package ivf.dao;

import org.springframework.data.repository.CrudRepository;

import ivf.data.ProtocolsUSEntity;

public interface ProtocolsUSDao extends CrudRepository<ProtocolsUSEntity, Long> {

}
