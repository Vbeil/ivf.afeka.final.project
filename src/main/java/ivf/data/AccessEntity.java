package ivf.data;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ivf.dataDeserializers.ExcelDateDeserializer;
import ivf.dataDeserializers.ExcelTimeDeserializer;
import ivf.dataDeserializers.FloatDeserializer;
import ivf.dataDeserializers.IntegerDeserializer;
import ivf.dataDeserializers.StringNullCleanerDeserializer;
import ivf.dataDeserializers.YesNoToBooleanDeserializer;

@Entity
@Table(name = "AccessData")
public class AccessEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long rowId;

	@JsonProperty("idHash")
	private String patientId;

	@JsonDeserialize(using = YesNoToBooleanDeserializer.class)
	@JsonProperty("chameleonIvfPatient")
	private Boolean chameleonIvfPatient;

	@JsonDeserialize(using = YesNoToBooleanDeserializer.class)
	@JsonProperty("chameleonPatient")
	private Boolean chameleonPatient;

	@JsonDeserialize(using = IntegerDeserializer.class)
	@JsonProperty("birthYy")
	private Integer birthYear;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	private LocalDate birthDateM;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	private LocalDate date;

	@JsonDeserialize(using = IntegerDeserializer.class)
	@JsonProperty("opuno")
	private Integer opuNo;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer opuTotal;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer opuFromBirth;

	@JsonDeserialize(using = YesNoToBooleanDeserializer.class)
	private Boolean embryoscope;

	@JsonDeserialize(using = YesNoToBooleanDeserializer.class)
	private Boolean thawedOocytes;

	@JsonDeserialize(using = YesNoToBooleanDeserializer.class)
	private Boolean icsi;

	@JsonDeserialize(using = YesNoToBooleanDeserializer.class)
	private Boolean ivf;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer infertil;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer priSec;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer g;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer p;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer sa;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer aa;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer ep;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String medium;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer oocytes;

	@JsonDeserialize(using = IntegerDeserializer.class)
	@JsonProperty("ivfoocytes")
	private Integer ivfOocytes;

	@JsonDeserialize(using = IntegerDeserializer.class)
	@JsonProperty("ivffertilized")
	private Integer ivfFertilized;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer immature;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer injected;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer intact;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer fertilized;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer et;

	@JsonDeserialize(using = FloatDeserializer.class)
	private Float frozen;

	@JsonDeserialize(using = YesNoToBooleanDeserializer.class)
	private Boolean preg;

	@JsonDeserialize(using = YesNoToBooleanDeserializer.class)
	private Boolean pesaTesaTese;

	@JsonDeserialize(using = YesNoToBooleanDeserializer.class)
	private Integer d; // TODO Problem? All NULL

	@JsonDeserialize(using = YesNoToBooleanDeserializer.class)
	private Integer d1; // TODO Problem? All NULL

	@JsonDeserialize(using = YesNoToBooleanDeserializer.class)
	private Integer frzSp; // TODO Problem? All NULL

	@JsonDeserialize(using = FloatDeserializer.class)
	private Float vol;

	@JsonDeserialize(using = FloatDeserializer.class)
	private Float count;

	@JsonDeserialize(using = FloatDeserializer.class)
	private Float motility;

	@JsonDeserialize(using = FloatDeserializer.class)
	private Float grade;

	@JsonDeserialize(using = FloatDeserializer.class)
	private Float su;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer cultured;

	@JsonDeserialize(using = FloatDeserializer.class)
	private Float noblastocysts;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer blastEt;

	@JsonDeserialize(using = FloatDeserializer.class)
	private Float blastFrozen;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer noOoctesDonated;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer ed; // TODO Problem? All NULL

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer d2;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer ah;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String protocol;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String drugs;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer noAmpules;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer iu;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer days;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer e2;

	@JsonDeserialize(using = FloatDeserializer.class)
	private Float prog;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer endomet;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer noMatureFoll;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer labNo;

	@JsonDeserialize(using = ExcelTimeDeserializer.class)
	private LocalTime opuTime;

	@JsonDeserialize(using = ExcelTimeDeserializer.class)
	private LocalTime hyalTime;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	private LocalDate beta1Date;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer valBeta1;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer progesterone1;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	private LocalDate beta2Date;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer valBeta2;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer progesterone2;

	@JsonDeserialize(using = IntegerDeserializer.class)
	@JsonProperty("ערךbeta3")
	private Integer valBeta3;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer progesterone3;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	private LocalDate usDate;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer sacCount;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer embryosCount;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer pulse;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	private LocalDate us2Date;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer sacCount2;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer embryosCount2;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer pulse2;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer pregnancyOutcome;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer pregnancyOutcomeWeek;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	@JsonProperty("testCvs")
	private String testCVS;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String amnioticFluid;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String skiratMarachot;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	private LocalDate deliveryDate;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer deliveryWeek;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String csVd;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String deliveryRemark;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer nb1Weight;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String nb1Gender;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer nb2Weight;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String nb2Gender;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer nb3Weight;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String nb3Gender;

	@JsonDeserialize(using = IntegerDeserializer.class)
	@JsonProperty("מומים")
	private Integer congenitalDisorders;

	@JsonDeserialize(using = ExcelTimeDeserializer.class)
	@JsonProperty("injecTime")
	private LocalTime injectTime;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String triggerType;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer weightM;

	@JsonDeserialize(using = IntegerDeserializer.class)
	@JsonProperty("hightM")
	private Integer heightM;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String fishPcr;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String opuid;

	@JsonDeserialize(using = FloatDeserializer.class)
	private Float lh;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer afc;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String cultureMedium;

	public AccessEntity() {
		
	}

	public AccessEntity(String patientId, Boolean chameleonIvfPatient, Boolean chameleonPatient, Integer birthYear,
			LocalDate birthDateM, LocalDate date, Integer opuNo, Integer opuTotal, Integer opuFromBirth,
			Boolean embryoscope, Boolean thawedOocytes, Boolean iCSI, Boolean iVF, Integer infertil, Integer priSec,
			Integer g, Integer p, Integer sa, Integer aa, Integer ep, String medium, Integer oocytes,
			Integer ivfOocytes, Integer ivfFertilized, Integer immature, Integer injected, Integer intact,
			Integer fertilized, Integer et, Float frozen, Boolean preg, Boolean pesaTesaTese, Integer d, Integer d1,
			Integer frzSp, Float vol, Float count, Float motility, Float grade, Float su, Integer cultured,
			Float noblastocysts, Integer blastEt, Float blastFrozen, Integer noOoctesDonated, Integer ed,
			Integer d2, Integer ah, String protocol, String drugs, Integer noAmpules, Integer iu, Integer days,
			Integer e2, Float prog, Integer endomet, Integer noMatureFoll, Integer labNo, LocalTime opuTime,
			LocalTime hyalTime, LocalDate beta1Date, Integer valBeta1, Integer progesterone1, LocalDate beta2Date,
			Integer valBeta2, Integer progesterone2, Integer valBeta3, Integer progesterone3, LocalDate usDate,
			Integer sacCount, Integer embryosCount, Integer pulse, LocalDate us2Date, Integer sacCount2,
			Integer embryosCount2, Integer pulse2, Integer pregnancyOutcome, Integer pregnancyOutcomeWeek,
			String testCVS, String amnioticFluid, String skiratMarachot, LocalDate deliveryDate, Integer deliveryWeek,
			String csVd, String deliveryRemark, Integer nb1Weight, String nb1Gender, Integer nb2Weight,
			String nb2Gender, Integer nb3Weight, String nb3Gender, Integer congenitalDisorders, LocalTime injectTime,
			String trigger, Integer weightM, Integer heightM, String fishPcr, String opuid, Float lh, Integer afc,
			String cultureMedium) {
		super();
		this.patientId = patientId;
		this.chameleonIvfPatient = chameleonIvfPatient;
		this.chameleonPatient = chameleonPatient;
		this.birthYear = birthYear;
		this.birthDateM = birthDateM;
		this.date = date;
		this.opuNo = opuNo;
		this.opuTotal = opuTotal;
		this.opuFromBirth = opuFromBirth;
		this.embryoscope = embryoscope;
		this.thawedOocytes = thawedOocytes;
		icsi = iCSI;
		ivf = iVF;
		this.infertil = infertil;
		this.priSec = priSec;
		this.g = g;
		this.p = p;
		this.sa = sa;
		this.aa = aa;
		this.ep = ep;
		this.medium = medium;
		this.oocytes = oocytes;
		this.ivfOocytes = ivfOocytes;
		this.ivfFertilized = ivfFertilized;
		this.immature = immature;
		this.injected = injected;
		this.intact = intact;
		this.fertilized = fertilized;
		this.et = et;
		this.frozen = frozen;
		this.preg = preg;
		this.pesaTesaTese = pesaTesaTese;
		this.d = d;
		this.d1 = d1;
		this.frzSp = frzSp;
		this.vol = vol;
		this.count = count;
		this.motility = motility;
		this.grade = grade;
		this.su = su;
		this.cultured = cultured;
		this.noblastocysts = noblastocysts;
		this.blastEt = blastEt;
		this.blastFrozen = blastFrozen;
		this.noOoctesDonated = noOoctesDonated;
		this.ed = ed;
		this.d2 = d2;
		this.ah = ah;
		this.protocol = protocol;
		this.drugs = drugs;
		this.noAmpules = noAmpules;
		this.iu = iu;
		this.days = days;
		this.e2 = e2;
		this.prog = prog;
		this.endomet = endomet;
		this.noMatureFoll = noMatureFoll;
		this.labNo = labNo;
		this.opuTime = opuTime;
		this.hyalTime = hyalTime;
		this.beta1Date = beta1Date;
		this.valBeta1 = valBeta1;
		this.progesterone1 = progesterone1;
		this.beta2Date = beta2Date;
		this.valBeta2 = valBeta2;
		this.progesterone2 = progesterone2;
		this.valBeta3 = valBeta3;
		this.progesterone3 = progesterone3;
		this.usDate = usDate;
		this.sacCount = sacCount;
		this.embryosCount = embryosCount;
		this.pulse = pulse;
		this.us2Date = us2Date;
		this.sacCount2 = sacCount2;
		this.embryosCount2 = embryosCount2;
		this.pulse2 = pulse2;
		this.pregnancyOutcome = pregnancyOutcome;
		this.pregnancyOutcomeWeek = pregnancyOutcomeWeek;
		this.testCVS = testCVS;
		this.amnioticFluid = amnioticFluid;
		this.skiratMarachot = skiratMarachot;
		this.deliveryDate = deliveryDate;
		this.deliveryWeek = deliveryWeek;
		this.csVd = csVd;
		this.deliveryRemark = deliveryRemark;
		this.nb1Weight = nb1Weight;
		this.nb1Gender = nb1Gender;
		this.nb2Weight = nb2Weight;
		this.nb2Gender = nb2Gender;
		this.nb3Weight = nb3Weight;
		this.nb3Gender = nb3Gender;
		this.congenitalDisorders = congenitalDisorders;
		this.injectTime = injectTime;
		this.triggerType = trigger;
		this.weightM = weightM;
		this.heightM = heightM;
		this.fishPcr = fishPcr;
		this.opuid = opuid;
		this.lh = lh;
		this.afc = afc;
		this.cultureMedium = cultureMedium;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public Boolean getChameleonIvfPatient() {
		return chameleonIvfPatient;
	}

	public void setChameleonIvfPatient(Boolean chameleonIvfPatient) {
		this.chameleonIvfPatient = chameleonIvfPatient;
	}

	public Boolean getChameleonPatient() {
		return chameleonPatient;
	}

	public void setChameleonPatient(Boolean chameleonPatient) {
		this.chameleonPatient = chameleonPatient;
	}

	public Integer getBirthYear() {
		return birthYear;
	}

	public void setBirthYear(Integer birthYear) {
		this.birthYear = birthYear;
	}

	public LocalDate getBirthDateM() {
		return birthDateM;
	}

	public void setBirthDateM(LocalDate birthDateM) {
		this.birthDateM = birthDateM;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public Integer getOpuNo() {
		return opuNo;
	}

	public void setOpuNo(Integer opuNo) {
		this.opuNo = opuNo;
	}

	public Integer getOpuTotal() {
		return opuTotal;
	}

	public void setOpuTotal(Integer opuTotal) {
		this.opuTotal = opuTotal;
	}

	public Integer getOpuFromBirth() {
		return opuFromBirth;
	}

	public void setOpuFromBirth(Integer opuFromBirth) {
		this.opuFromBirth = opuFromBirth;
	}

	public Boolean getEmbryoscope() {
		return embryoscope;
	}

	public void setEmbryoscope(Boolean embryoscope) {
		this.embryoscope = embryoscope;
	}

	public Boolean getThawedOocytes() {
		return thawedOocytes;
	}

	public void setThawedOocytes(Boolean thawedOocytes) {
		this.thawedOocytes = thawedOocytes;
	}

	public Boolean getICSI() {
		return icsi;
	}

	public void setICSI(Boolean iCSI) {
		icsi = iCSI;
	}

	public Boolean getIVF() {
		return ivf;
	}

	public void setIVF(Boolean iVF) {
		ivf = iVF;
	}

	public Integer getInfertil() {
		return infertil;
	}

	public void setInfertil(Integer infertil) {
		this.infertil = infertil;
	}

	public Integer getPriSec() {
		return priSec;
	}

	public void setPriSec(Integer priSec) {
		this.priSec = priSec;
	}

	public Integer getG() {
		return g;
	}

	public void setG(Integer g) {
		this.g = g;
	}

	public Integer getP() {
		return p;
	}

	public void setP(Integer p) {
		this.p = p;
	}

	public Integer getSa() {
		return sa;
	}

	public void setSa(Integer sa) {
		this.sa = sa;
	}

	public Integer getAa() {
		return aa;
	}

	public void setAa(Integer aa) {
		this.aa = aa;
	}

	public Integer getEp() {
		return ep;
	}

	public void setEp(Integer ep) {
		this.ep = ep;
	}

	public String getMedium() {
		return medium;
	}

	public void setMedium(String medium) {
		this.medium = medium;
	}

	public Integer getOocytes() {
		return oocytes;
	}

	public void setOocytes(Integer oocytes) {
		this.oocytes = oocytes;
	}

	public Integer getIvfOocytes() {
		return ivfOocytes;
	}

	public void setIvfOocytes(Integer ivfOocytes) {
		this.ivfOocytes = ivfOocytes;
	}

	public Integer getIvfFertilized() {
		return ivfFertilized;
	}

	public void setIvfFertilized(Integer ivfFertilized) {
		this.ivfFertilized = ivfFertilized;
	}

	public Integer getImmature() {
		return immature;
	}

	public void setImmature(Integer immature) {
		this.immature = immature;
	}

	public Integer getInjected() {
		return injected;
	}

	public void setInjected(Integer injected) {
		this.injected = injected;
	}

	public Integer getIntact() {
		return intact;
	}

	public void setIntact(Integer intact) {
		this.intact = intact;
	}

	public Integer getFertilized() {
		return fertilized;
	}

	public void setFertilized(Integer fertilized) {
		this.fertilized = fertilized;
	}

	public Integer getEt() {
		return et;
	}

	public void setEt(Integer et) {
		this.et = et;
	}

	public Float getFrozen() {
		return frozen;
	}

	public void setFrozen(Float frozen) {
		this.frozen = frozen;
	}

	public Boolean getPreg() {
		return preg;
	}

	public void setPreg(Boolean preg) {
		this.preg = preg;
	}

	public Boolean getPesaTesaTese() {
		return pesaTesaTese;
	}

	public void setPesaTesaTese(Boolean pesaTesaTese) {
		this.pesaTesaTese = pesaTesaTese;
	}

	public Integer getD() {
		return d;
	}

	public void setD(Integer d) {
		this.d = d;
	}

	public Integer getD1() {
		return d1;
	}

	public void setD1(Integer d1) {
		this.d1 = d1;
	}

	public Integer getFrzSp() {
		return frzSp;
	}

	public void setFrzSp(Integer frzSp) {
		this.frzSp = frzSp;
	}

	public Float getVol() {
		return vol;
	}

	public void setVol(Float vol) {
		this.vol = vol;
	}

	public Float getCount() {
		return count;
	}

	public void setCount(Float count) {
		this.count = count;
	}

	public Float getMotility() {
		return motility;
	}

	public void setMotility(Float motility) {
		this.motility = motility;
	}

	public Float getGrade() {
		return grade;
	}

	public void setGrade(Float grade) {
		this.grade = grade;
	}

	public Float getSu() {
		return su;
	}

	public void setSu(Float su) {
		this.su = su;
	}

	public Integer getCultured() {
		return cultured;
	}

	public void setCultured(Integer cultured) {
		this.cultured = cultured;
	}

	public Float getNoblastocysts() {
		return noblastocysts;
	}

	public void setNoblastocysts(Float noblastocysts) {
		this.noblastocysts = noblastocysts;
	}

	public Integer getBlastEt() {
		return blastEt;
	}

	public void setBlastEt(Integer blastEt) {
		this.blastEt = blastEt;
	}

	public Float getBlastFrozen() {
		return blastFrozen;
	}

	public void setBlastFrozen(Float blastFrozen) {
		this.blastFrozen = blastFrozen;
	}

	public Integer getNoOoctesDonated() {
		return noOoctesDonated;
	}

	public void setNoOoctesDonated(Integer noOoctesDonated) {
		this.noOoctesDonated = noOoctesDonated;
	}

	public Integer getEd() {
		return ed;
	}

	public void setEd(Integer ed) {
		this.ed = ed;
	}

	public Integer getD2() {
		return d2;
	}

	public void setD2(Integer d2) {
		this.d2 = d2;
	}

	public Integer getAh() {
		return ah;
	}

	public void setAh(Integer ah) {
		this.ah = ah;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getDrugs() {
		return drugs;
	}

	public void setDrugs(String drugs) {
		this.drugs = drugs;
	}

	public Integer getNoAmpules() {
		return noAmpules;
	}

	public void setNoAmpules(Integer noAmpules) {
		this.noAmpules = noAmpules;
	}

	public Integer getIu() {
		return iu;
	}

	public void setIu(Integer iu) {
		this.iu = iu;
	}

	public Integer getDays() {
		return days;
	}

	public void setDays(Integer days) {
		this.days = days;
	}

	public Integer getE2() {
		return e2;
	}

	public void setE2(Integer e2) {
		this.e2 = e2;
	}

	public Float getProg() {
		return prog;
	}

	public void setProg(Float prog) {
		this.prog = prog;
	}

	public Integer getEndomet() {
		return endomet;
	}

	public void setEndomet(Integer endomet) {
		this.endomet = endomet;
	}

	public Integer getNoMatureFoll() {
		return noMatureFoll;
	}

	public void setNoMatureFoll(Integer noMatureFoll) {
		this.noMatureFoll = noMatureFoll;
	}

	public Integer getLabNo() {
		return labNo;
	}

	public void setLabNo(Integer labNo) {
		this.labNo = labNo;
	}

	public LocalTime getOpuTime() {
		return opuTime;
	}

	public void setOpuTime(LocalTime opuTime) {
		this.opuTime = opuTime;
	}

	public LocalTime getHyalTime() {
		return hyalTime;
	}

	public void setHyalTime(LocalTime hyalTime) {
		this.hyalTime = hyalTime;
	}

	public LocalDate getBeta1Date() {
		return beta1Date;
	}

	public void setBeta1Date(LocalDate beta1Date) {
		this.beta1Date = beta1Date;
	}

	public Integer getValBeta1() {
		return valBeta1;
	}

	public void setValBeta1(Integer valBeta1) {
		this.valBeta1 = valBeta1;
	}

	public Integer getProgesterone1() {
		return progesterone1;
	}

	public void setProgesterone1(Integer progesterone1) {
		this.progesterone1 = progesterone1;
	}

	public LocalDate getBeta2Date() {
		return beta2Date;
	}

	public void setBeta2Date(LocalDate beta2Date) {
		this.beta2Date = beta2Date;
	}

	public Integer getValBeta2() {
		return valBeta2;
	}

	public void setValBeta2(Integer valBeta2) {
		this.valBeta2 = valBeta2;
	}

	public Integer getProgesterone2() {
		return progesterone2;
	}

	public void setProgesterone2(Integer progesterone2) {
		this.progesterone2 = progesterone2;
	}

	public Integer getValBeta3() {
		return valBeta3;
	}

	public void setValBeta3(Integer valBeta3) {
		this.valBeta3 = valBeta3;
	}

	public Integer getProgesterone3() {
		return progesterone3;
	}

	public void setProgesterone3(Integer progesterone3) {
		this.progesterone3 = progesterone3;
	}

	public LocalDate getUsDate() {
		return usDate;
	}

	public void setUsDate(LocalDate usDate) {
		this.usDate = usDate;
	}

	public Integer getSacCount() {
		return sacCount;
	}

	public void setSacCount(Integer sacCount) {
		this.sacCount = sacCount;
	}

	public Integer getEmbryosCount() {
		return embryosCount;
	}

	public void setEmbryosCount(Integer embryosCount) {
		this.embryosCount = embryosCount;
	}

	public Integer getPulse() {
		return pulse;
	}

	public void setPulse(Integer pulse) {
		this.pulse = pulse;
	}

	public LocalDate getUs2Date() {
		return us2Date;
	}

	public void setUs2Date(LocalDate us2Date) {
		this.us2Date = us2Date;
	}

	public Integer getSacCount2() {
		return sacCount2;
	}

	public void setSacCount2(Integer sacCount2) {
		this.sacCount2 = sacCount2;
	}

	public Integer getEmbryosCount2() {
		return embryosCount2;
	}

	public void setEmbryosCount2(Integer embryosCount2) {
		this.embryosCount2 = embryosCount2;
	}

	public Integer getPulse2() {
		return pulse2;
	}

	public void setPulse2(Integer pulse2) {
		this.pulse2 = pulse2;
	}

	public Integer getPregnancyOutcome() {
		return pregnancyOutcome;
	}

	public void setPregnancyOutcome(Integer pregnancyOutcome) {
		this.pregnancyOutcome = pregnancyOutcome;
	}

	public Integer getPregnancyOutcomeWeek() {
		return pregnancyOutcomeWeek;
	}

	public void setPregnancyOutcomeWeek(Integer pregnancyOutcomeWeek) {
		this.pregnancyOutcomeWeek = pregnancyOutcomeWeek;
	}

	public String getTestCVS() {
		return testCVS;
	}

	public void setTestCVS(String testCVS) {
		this.testCVS = testCVS;
	}

	public String getAmnioticFluid() {
		return amnioticFluid;
	}

	public void setAmnioticFluid(String amnioticFluid) {
		this.amnioticFluid = amnioticFluid;
	}

	public String getSkiratMarachot() {
		return skiratMarachot;
	}

	public void setSkiratMarachot(String skiratMarachot) {
		this.skiratMarachot = skiratMarachot;
	}

	public LocalDate getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(LocalDate deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public Integer getDeliveryWeek() {
		return deliveryWeek;
	}

	public void setDeliveryWeek(Integer deliveryWeek) {
		this.deliveryWeek = deliveryWeek;
	}

	public String getCsVd() {
		return csVd;
	}

	public void setCsVd(String csVd) {
		this.csVd = csVd;
	}

	public String getDeliveryRemark() {
		return deliveryRemark;
	}

	public void setDeliveryRemark(String deliveryRemark) {
		this.deliveryRemark = deliveryRemark;
	}

	public Integer getNb1Weight() {
		return nb1Weight;
	}

	public void setNb1Weight(Integer nb1Weight) {
		this.nb1Weight = nb1Weight;
	}

	public String getNb1Gender() {
		return nb1Gender;
	}

	public void setNb1Gender(String nb1Gender) {
		this.nb1Gender = nb1Gender;
	}

	public Integer getNb2Weight() {
		return nb2Weight;
	}

	public void setNb2Weight(Integer nb2Weight) {
		this.nb2Weight = nb2Weight;
	}

	public String getNb2Gender() {
		return nb2Gender;
	}

	public void setNb2Gender(String nb2Gender) {
		this.nb2Gender = nb2Gender;
	}

	public Integer getNb3Weight() {
		return nb3Weight;
	}

	public void setNb3Weight(Integer nb3Weight) {
		this.nb3Weight = nb3Weight;
	}

	public String getNb3Gender() {
		return nb3Gender;
	}

	public void setNb3Gender(String nb3Gender) {
		this.nb3Gender = nb3Gender;
	}

	public Integer getCongenitalDisorders() {
		return congenitalDisorders;
	}

	public void setCongenitalDisorders(Integer congenitalDisorders) {
		this.congenitalDisorders = congenitalDisorders;
	}

	public LocalTime getInjectTime() {
		return injectTime;
	}

	public void setInjectTime(LocalTime injectTime) {
		this.injectTime = injectTime;
	}

	public String getTrigger() {
		return triggerType;
	}

	public void setTrigger(String trigger) {
		this.triggerType = trigger;
	}

	public Integer getWeightM() {
		return weightM;
	}

	public void setWeightM(Integer weightM) {
		this.weightM = weightM;
	}

	public Integer getHeightM() {
		return heightM;
	}

	public void setHeightM(Integer heightM) {
		this.heightM = heightM;
	}

	public String getFishPcr() {
		return fishPcr;
	}

	public void setFishPcr(String fishPcr) {
		this.fishPcr = fishPcr;
	}

	public String getOpuid() {
		return opuid;
	}

	public void setOpuid(String opuid) {
		this.opuid = opuid;
	}

	public Float getLh() {
		return lh;
	}

	public void setLh(Float lh) {
		this.lh = lh;
	}

	public Integer getAfc() {
		return afc;
	}

	public void setAfc(Integer afc) {
		this.afc = afc;
	}

	public String getCultureMedium() {
		return cultureMedium;
	}

	public void setCultureMedium(String cultureMedium) {
		this.cultureMedium = cultureMedium;
	}

}
