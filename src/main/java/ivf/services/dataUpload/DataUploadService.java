package ivf.services.dataUpload;

import ivf.data.AccessEntity;
import ivf.data.BmiEntity;
import ivf.data.ComorbiditsEntity;
import ivf.data.DiagnosisEntity;
import ivf.data.FemaleMaleConnectionEntity;
import ivf.data.HabitsEntity;
import ivf.data.IVFHistoryEntity;
import ivf.data.MaleEntity;
import ivf.data.OPUDrugsEntity;
import ivf.data.OPUEntity;
import ivf.data.OtherTestsEntity;
import ivf.data.OvumFollowupEntity;
import ivf.data.PatientEntity;
import ivf.data.ProtocolsDrugsEntity;
import ivf.data.ProtocolsEntity;
import ivf.data.ProtocolsFollowupEntity;
import ivf.data.ProtocolsProceduresEntity;
import ivf.data.ProtocolsUSEntity;
import ivf.data.PuStorageEntity;
import ivf.data.ReturnActionDrugsEntity;
import ivf.data.ReturnActionEntity;
import ivf.data.SpermEntity;
import ivf.data.TreatmentSummaryEntity;
import ivf.data.UsPregnancyEntity;

public interface DataUploadService {

	/**
	 * Upload Table #1 - Visits
	 *
	 * @param patients The Patients array parsed from the Excel table
	 * @param override Determines if the database should be cleared first
	 */
	void uploadVisitsTable(PatientEntity[] patients, String override);

	/**
	 * Upload Table #2 - IVF History
	 *
	 * @param ivfHistoryEntries The previous IVF History array parsed from the Excel
	 *                          table
	 * @param override          Determines if the database should be cleared first
	 */
	void uploadIVFHistoryTable(IVFHistoryEntity[] ivfHistoryEntries, String override);

	/**
	 * Upload Table #3 - Other Tests
	 *
	 * @param otherTestsEntries The other tests array parsed from the Excel table
	 * @param override          Determines if the database should be cleared first
	 */
	void uploadOtherTestsTable(OtherTestsEntity[] otherTestsEntries, String override);

	/**
	 * Upload Table #4 - Diagnosis
	 *
	 * @param diagnosisEntries The diagnosis array parsed from the Excel table
	 * @param override         Determines if the database should be cleared first
	 */
	void uploadDiagnosisTable(DiagnosisEntity[] diagnosisEntries, String override);

	/**
	 * Upload Table #5 - Protocols
	 *
	 * @param protocolEntries The protocols array parsed from the Excel table
	 * @param override        Determines if the database should be cleared first
	 */
	void uploadProtocolsTable(ProtocolsEntity[] protocolEntries, String override);

	/**
	 * Upload Table #5.1 - Protocols Followup
	 *
	 * @param protocolFollowupEntries The protocols followup array parsed from the
	 *                                Excel table
	 * @param override                Determines if the database should be cleared
	 *                                first
	 */
	void uploadProtocolsFollowupTable(ProtocolsFollowupEntity[] protocolFollowupEntries, String override);

	/**
	 * Upload Table #5.2 - Protocols Procedures
	 *
	 * @param protocolProceduresEntries The protocols procedures array parsed from
	 *                                  the Excel table
	 * @param override                  Determines if the database should be cleared
	 *                                  first
	 */
	void uploadProtocolsProceduresTable(ProtocolsProceduresEntity[] protocolProceduresEntries, String override);

	/**
	 * Upload Table #5.3 - Protocols US
	 *
	 * @param protocolUsEntries The protocols us array parsed from the Excel table
	 * @param override          Determines if the database should be cleared first
	 */
	void uploadProtocolsUSTable(ProtocolsUSEntity[] protocolUsEntries, String override);

	/**
	 * Upload Table #5.4 - Protocols Drugs
	 *
	 * @param protocolDrugsEntries The protocols drugs array parsed from the Excel
	 *                             table
	 * @param override             Determines if the database should be cleared
	 *                             first
	 */
	void uploadProtocolDrugsTable(ProtocolsDrugsEntity[] protocolDrugsEntries, String override);

	/**
	 * Upload Table #6 - OPU
	 *
	 * @param opuEntries The OPU array parsed from the Excel table
	 * @param override   Determines if the database should be cleared first
	 */
	void uploadOPUTable(OPUEntity[] opuEntries, String override);

	/**
	 * Upload Table #6.1 - OPU Drugs
	 *
	 * @param opuDrugsEntries The OPU drugs array parsed from the Excel table
	 * @param override        Determines if the database should be cleared first
	 */
	void uploadOPUDrugsTable(OPUDrugsEntity[] opuDrugsEntries, String override);

	/**
	 * Upload Table #7 - Return Action
	 *
	 * @param returnActionEntries The return actions array parsed from the Excel
	 *                            table
	 * @param override            Determines if the database should be cleared first
	 */
	void uploadReturnActionTable(ReturnActionEntity[] returnActionEntries, String override);

	/**
	 * Upload Table #7.1 - Return Action Drugs
	 *
	 * @param returnActionDrugsEntries The return actions drugs array parsed from
	 *                                 the Excel table
	 * @param override                 Determines if the database should be cleared
	 *                                 first
	 */
	void uploadReturnActionDrugsTable(ReturnActionDrugsEntity[] returnActionDrugsEntries, String override);

	/**
	 * Upload Table #8 - Sperm
	 *
	 * @param spermEntries The Sperm array parsed from the Excel table
	 * @param override     Determines if the database should be cleared first
	 */
	void uploadSpermTable(SpermEntity[] spermEntries, String override);

	/**
	 * Upload Table #9 - Ovum Followup
	 *
	 * @param ovumFollowupEntries The Ovum Followup array parsed from the Excel
	 *                            table
	 * @param override            Determines if the database should be cleared first
	 */
	void uploadOvumFollowupTable(OvumFollowupEntity[] ovumFollowupEntries, String override);

	/**
	 * Upload Table #10 - Pu Storage
	 *
	 * @param puStorageEntries The Pu Storage Entries array parsed from the Excel
	 *                         table
	 * @param override         Determines if the database should be cleared first
	 */
	void uploadPuStorageTable(PuStorageEntity[] puStorageEntries, String override);

	/**
	 * Upload Table #11 - Treatment Summary Connection
	 *
	 * @param treatmentSummaryEntries The treatment Summary Entries array parsed
	 *                                from the Excel table
	 * @param override                Determines if the database should be cleared
	 *                                first
	 */
	void uploadTreatmentSummaryTable(TreatmentSummaryEntity[] treatmentSummaryEntries, String override);

	/**
	 * Upload Table #11.1 - Us Pregnancy
	 *
	 * @param usPregnancyEntries The Us Pregnancy Entries array parsed from the
	 *                           Excel table
	 * @param override           Determines if the database should be cleared first
	 */
	void uploadUsPregnancyTable(UsPregnancyEntity[] usPregnancyEntries, String override);

	/**
	 * Upload Table #12 - Male
	 *
	 * @param maleEntries The male Entries array parsed from the Excel table
	 * @param override    Determines if the database should be cleared first
	 */
	void uploadMaleTable(MaleEntity[] maleEntries, String override);

	/**
	 * Upload Table #13 - Comorbidits
	 *
	 * @param comorbiditsEntries The Comorbidits entities array parsed from the
	 *                           Habits table
	 * @param override           Determines if the database should be cleared first
	 */
	void uploadComorbiditsTable(ComorbiditsEntity[] comorbiditsEntries, String override);

	/**
	 * Upload Table #14 - Habits
	 *
	 * @param habitsEntries The Habits entities array parsed from the Habits table
	 * @param override      Determines if the database should be cleared first
	 */
	void uploadHabitsTable(HabitsEntity[] habitsEntries, String override);

	/**
	 * Upload Table #15 - BMI
	 *
	 * @param bmiEntries The BMI entities array parsed from the BMI table
	 * @param override   Determines if the database should be cleared first
	 */
	void uploadBmiTable(BmiEntity[] bmiEntries, String override);

	/**
	 * Upload Table #16 - Access Data
	 *
	 * @param accessPatients The Access Data entities array parsed from the Excel
	 *                       table
	 * @param override       Determines if the database should be cleared first
	 */
	void uploadAccessTable(AccessEntity[] accessPatients, String override);

	/**
	 * Upload Table #17 - Female Male Connection
	 *
	 * @param femaleMaleConnectionEntries The female male connections array parsed
	 *                                    from the Excel table
	 * @param override                    Determines if the database should be
	 *                                    cleared first
	 */
	void uploadFemaleMaleConnectionTable(FemaleMaleConnectionEntity[] femaleMaleConnectionEntries, String override);

	/**
	 * Clears the database and the cache, should be used in case of data corruption
	 */
	void clearDatabaseAndCache();
}
