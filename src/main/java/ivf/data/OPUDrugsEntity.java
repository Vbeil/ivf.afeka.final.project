package ivf.data;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ivf.dataDeserializers.ExcelDateDeserializer;
import ivf.dataDeserializers.ExcelDateTimeDeserializer;
import ivf.dataDeserializers.FloatDeserializer;
import ivf.dataDeserializers.IntegerDeserializer;
import ivf.dataDeserializers.StringNullCleanerDeserializer;

@Entity
@Table(name = "OPU_Drugs")
public class OPUDrugsEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long rowId;

	@JsonProperty("idHash")
	private String patientId;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	@JsonProperty("admissionNoHash")
	private String admission;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	private LocalDate plannedActionDate;

	@JsonDeserialize(using = ExcelDateTimeDeserializer.class)
	private LocalDateTime opuDate;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String basicName;

	@JsonDeserialize(using = FloatDeserializer.class)
	private Float dosage;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer dosageUnits;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer units;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer times;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer timesPer;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer duration;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer durationUnits;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String durationUnitsText;

	public OPUDrugsEntity() {
	}

	public OPUDrugsEntity(String patientId, String admission, LocalDate plannedActionDate, LocalDateTime opuDate,
			String basicName, Float dosage, Integer dosageUnits, Integer units, Integer times, Integer timesPer,
			Integer duration, Integer durationUnits, String durationUnitsText) {
		super();
		this.patientId = patientId;
		this.admission = admission;
		this.plannedActionDate = plannedActionDate;
		this.opuDate = opuDate;
		this.basicName = basicName;
		this.dosage = dosage;
		this.dosageUnits = dosageUnits;
		this.units = units;
		this.times = times;
		this.timesPer = timesPer;
		this.duration = duration;
		this.durationUnits = durationUnits;
		this.durationUnitsText = durationUnitsText;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public String getAdmission() {
		return admission;
	}

	public void setAdmission(String admission) {
		this.admission = admission;
	}

	public LocalDate getPlannedActionDate() {
		return plannedActionDate;
	}

	public void setPlannedActionDate(LocalDate plannedActionDate) {
		this.plannedActionDate = plannedActionDate;
	}

	public LocalDateTime getOpuDate() {
		return opuDate;
	}

	public void setOpuDate(LocalDateTime opuDate) {
		this.opuDate = opuDate;
	}

	public String getBasicName() {
		return basicName;
	}

	public void setBasicName(String basicName) {
		this.basicName = basicName;
	}

	public Float getDosage() {
		return dosage;
	}

	public void setDosage(Float dosage) {
		this.dosage = dosage;
	}

	public Integer getDosageUnits() {
		return dosageUnits;
	}

	public void setDosageUnits(Integer dosageUnits) {
		this.dosageUnits = dosageUnits;
	}

	public Integer getUnits() {
		return units;
	}

	public void setUnits(Integer units) {
		this.units = units;
	}

	public Integer getTimes() {
		return times;
	}

	public void setTimes(Integer times) {
		this.times = times;
	}

	public Integer getTimesPer() {
		return timesPer;
	}

	public void setTimesPer(Integer timesPer) {
		this.timesPer = timesPer;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Integer getDurationUnits() {
		return durationUnits;
	}

	public void setDurationUnits(Integer durationUnits) {
		this.durationUnits = durationUnits;
	}

	public String getDurationUnitsText() {
		return durationUnitsText;
	}

	public void setDurationUnitsText(String durationUnitsText) {
		this.durationUnitsText = durationUnitsText;
	}

}
