package ivf.services.queryCache;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public interface QueryCacheService {

	/**
	 * Returns a query result if present in the cache, executes the query otherwise.
	 * 
	 * @param key A SQL query
	 * @return A result from the database based on the query provided
	 * @throws ExecutionException
	 */
	public List<Map<String, Object>> getFromQueryCache(QueryCacheKey key) throws ExecutionException;

	/**
	 * Clears the cache, should be used when uploading new data.
	 */
	void clearQueryCache();
}
