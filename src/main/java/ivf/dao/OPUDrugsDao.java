package ivf.dao;

import org.springframework.data.repository.CrudRepository;

import ivf.data.OPUDrugsEntity;

public interface OPUDrugsDao extends CrudRepository<OPUDrugsEntity, Long> {

}
