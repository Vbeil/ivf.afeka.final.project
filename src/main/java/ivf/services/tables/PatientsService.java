package ivf.services.tables;

import java.util.List;

import ivf.data.PatientEntity;

public interface PatientsService {

	public PatientEntity create(PatientEntity newPatient) throws Exception;
	
	public PatientEntity[] create(PatientEntity[] newPatients) throws Exception;

	public PatientEntity update(Long patientId, PatientEntity update) throws Exception;
	
	public List<PatientEntity> getAll();
	
	public PatientEntity getSpecificPatient(Long patientId) throws Exception;
	
	public void deleteAll();
	
}

