package ivf.data;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ivf.dataDeserializers.ExcelDateDeserializer;
import ivf.dataDeserializers.FloatDeserializer;
import ivf.dataDeserializers.StringNullCleanerDeserializer;

@Entity
@Table(name = "Bmi")
public class BmiEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long rowId;

	@JsonProperty("idHash")
	private String patientId;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	@JsonProperty("genderText")
	private String gender;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String birthDateYear;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	private LocalDate entryDate;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String parameterName;

	@JsonDeserialize(using = FloatDeserializer.class)
	private Float result;

	public BmiEntity() {

	}

	public BmiEntity(String patientId, String gender, String birthDateYear, LocalDate entryDate, String parameterName,
			Float result) {
		super();
		this.patientId = patientId;
		this.gender = gender;
		this.birthDateYear = birthDateYear;
		this.entryDate = entryDate;
		this.parameterName = parameterName;
		this.result = result;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBirthDateYear() {
		return birthDateYear;
	}

	public void setBirthDateYear(String birthDateYear) {
		this.birthDateYear = birthDateYear;
	}

	public LocalDate getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(LocalDate entryDate) {
		this.entryDate = entryDate;
	}

	public String getParameterName() {
		return parameterName;
	}

	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}

	public Float getResult() {
		return result;
	}

	public void setResult(Float result) {
		this.result = result;
	}

}
