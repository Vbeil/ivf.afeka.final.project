package ivf.dao;

import org.springframework.data.repository.CrudRepository;

import ivf.data.TreatmentSummaryEntity;

public interface TreatmentSummaryDao extends CrudRepository<TreatmentSummaryEntity, Long> {

}
