package ivf.dataDeserializers;

import java.io.IOException;
import java.time.LocalDate;
import java.time.Month;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class ExcelDateDeserializer extends JsonDeserializer<LocalDate> {

	@Override
	public LocalDate deserialize(JsonParser parser, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {

		String dateStr = parser.getText();

		if (dateStr.isEmpty() || dateStr == null || dateStr.equalsIgnoreCase("NULL")) {
			return null;
		}

		LocalDate formattedDate = LocalDate.of(1899, Month.DECEMBER, 30).plusDays((long) Double.parseDouble(dateStr));

		return formattedDate;
	}
}
