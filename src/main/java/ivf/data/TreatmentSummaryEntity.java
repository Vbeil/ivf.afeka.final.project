package ivf.data;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ivf.dataDeserializers.ExcelDateDeserializer;
import ivf.dataDeserializers.IntegerDeserializer;
import ivf.dataDeserializers.StringNullCleanerDeserializer;
import ivf.dataDeserializers.YesNoToBooleanDeserializer;

@Entity
@Table(name = "TreatmentSummary")
public class TreatmentSummaryEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long rowId;

	@JsonProperty("idHash")
	private String patientId;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String b1;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	private LocalDate dayB1;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String b1Val;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String b2;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	@JsonProperty("dayb2")
	private LocalDate dayB2;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String b2Val;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String b3;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	private LocalDate dayB3;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String b3Val;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	private LocalDate dayReturn;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	private LocalDate dayReturn2;

	@JsonDeserialize(using = YesNoToBooleanDeserializer.class)
	@JsonProperty("pregnancy1Y2N")
	private Boolean pregnancy;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	private LocalDate pregnancyDate;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	private LocalDate pregnancyStartDate;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	private LocalDate testDate;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer calcType;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String calcTypeText;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer fetusCount;

	public TreatmentSummaryEntity() {

	}

	public TreatmentSummaryEntity(String patientId, String b1, LocalDate dayB1, String b1Val, String b2,
			LocalDate dayB2, String b2Val, String b3, LocalDate dayB3, String b3Val, LocalDate dayReturn,
			LocalDate dayReturn2, Boolean pregnancy, LocalDate pregnancyDate, LocalDate pregnancyStartDate,
			LocalDate testDate, Integer calcType, String calcTypeText, Integer fetusCount) {
		super();
		this.patientId = patientId;
		this.b1 = b1;
		this.dayB1 = dayB1;
		this.b1Val = b1Val;
		this.b2 = b2;
		this.dayB2 = dayB2;
		this.b2Val = b2Val;
		this.b3 = b3;
		this.dayB3 = dayB3;
		this.b3Val = b3Val;
		this.dayReturn = dayReturn;
		this.dayReturn2 = dayReturn2;
		this.pregnancy = pregnancy;
		this.pregnancyDate = pregnancyDate;
		this.pregnancyStartDate = pregnancyStartDate;
		this.testDate = testDate;
		this.calcType = calcType;
		this.calcTypeText = calcTypeText;
		this.fetusCount = fetusCount;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public String getB1() {
		return b1;
	}

	public void setB1(String b1) {
		this.b1 = b1;
	}

	public LocalDate getDayB1() {
		return dayB1;
	}

	public void setDayB1(LocalDate dayB1) {
		this.dayB1 = dayB1;
	}

	public String getB1Val() {
		return b1Val;
	}

	public void setB1Val(String b1Val) {
		this.b1Val = b1Val;
	}

	public String getB2() {
		return b2;
	}

	public void setB2(String b2) {
		this.b2 = b2;
	}

	public LocalDate getDayB2() {
		return dayB2;
	}

	public void setDayB2(LocalDate dayB2) {
		this.dayB2 = dayB2;
	}

	public String getB2Val() {
		return b2Val;
	}

	public void setB2Val(String b2Val) {
		this.b2Val = b2Val;
	}

	public String getB3() {
		return b3;
	}

	public void setB3(String b3) {
		this.b3 = b3;
	}

	public LocalDate getDayB3() {
		return dayB3;
	}

	public void setDayB3(LocalDate dayB3) {
		this.dayB3 = dayB3;
	}

	public String getB3Val() {
		return b3Val;
	}

	public void setB3Val(String b3Val) {
		this.b3Val = b3Val;
	}

	public LocalDate getDayReturn() {
		return dayReturn;
	}

	public void setDayReturn(LocalDate dayReturn) {
		this.dayReturn = dayReturn;
	}

	public LocalDate getDayReturn2() {
		return dayReturn2;
	}

	public void setDayReturn2(LocalDate dayReturn2) {
		this.dayReturn2 = dayReturn2;
	}

	public Boolean getPregnancy() {
		return pregnancy;
	}

	public void setPregnancy(Boolean pregnancy) {
		this.pregnancy = pregnancy;
	}

	public LocalDate getPregnancyDate() {
		return pregnancyDate;
	}

	public void setPregnancyDate(LocalDate pregnancyDate) {
		this.pregnancyDate = pregnancyDate;
	}

	public LocalDate getPregnancyStartDate() {
		return pregnancyStartDate;
	}

	public void setPregnancyStartDate(LocalDate pregnancyStartDate) {
		this.pregnancyStartDate = pregnancyStartDate;
	}

	public LocalDate getTestDate() {
		return testDate;
	}

	public void setTestDate(LocalDate testDate) {
		this.testDate = testDate;
	}

	public Integer getCalcType() {
		return calcType;
	}

	public void setCalcType(Integer calcType) {
		this.calcType = calcType;
	}

	public String getCalcTypeText() {
		return calcTypeText;
	}

	public void setCalcTypeText(String calcTypeText) {
		this.calcTypeText = calcTypeText;
	}

	public Integer getFetusCount() {
		return fetusCount;
	}

	public void setFetusCount(Integer fetusCount) {
		this.fetusCount = fetusCount;
	}
}
