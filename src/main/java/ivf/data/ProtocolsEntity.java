package ivf.data;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ivf.dataDeserializers.ExcelDateDeserializer;
import ivf.dataDeserializers.ExcelDateTimeDeserializer;
import ivf.dataDeserializers.StringNullCleanerDeserializer;

@Entity
@Table(name = "Protocols")
public class ProtocolsEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long rowId;

	@JsonProperty("idHash")
	private String patientId;

	@JsonDeserialize(using = ExcelDateTimeDeserializer.class)
	private LocalDateTime entryDate;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	@JsonProperty("admissionNoHash")
	private String admission;

	@JsonProperty("protocol")
	private Integer protocolCode;

	@JsonProperty("protocolText")
	private String protocolName;

	@JsonDeserialize(using = ExcelDateTimeDeserializer.class)
	private LocalDateTime protocolEntryDate;

	private String fertilizationType;

	private Integer carePlanId;

	private String sperm;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	private LocalDate day1;

	private Integer dayTechNum; // ?
	
	public ProtocolsEntity() {
		
	}

	public ProtocolsEntity(String patientId, LocalDateTime entryDate, String admission, Integer protocolCode,
			String protocolName, LocalDateTime protocolEntryDate, String fertilizationType, Integer carePlanId,
			String sperm, LocalDate day1, Integer dayTechNum) {
		super();
		this.patientId = patientId;
		this.entryDate = entryDate;
		this.admission = admission;
		this.protocolCode = protocolCode;
		this.protocolName = protocolName;
		this.protocolEntryDate = protocolEntryDate;
		this.fertilizationType = fertilizationType;
		this.carePlanId = carePlanId;
		this.sperm = sperm;
		this.day1 = day1;
		this.dayTechNum = dayTechNum;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public LocalDateTime getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(LocalDateTime entryDate) {
		this.entryDate = entryDate;
	}

	public String getAdmission() {
		return admission;
	}

	public void setAdmission(String admission) {
		this.admission = admission;
	}

	public Integer getProtocolCode() {
		return protocolCode;
	}

	public void setProtocolCode(Integer protocolCode) {
		this.protocolCode = protocolCode;
	}

	public String getProtocolName() {
		return protocolName;
	}

	public void setProtocolName(String protocolName) {
		this.protocolName = protocolName;
	}

	public LocalDateTime getProtocolEntryDate() {
		return protocolEntryDate;
	}

	public void setProtocolEntryDate(LocalDateTime protocolEntryDate) {
		this.protocolEntryDate = protocolEntryDate;
	}

	public String getFertilizationType() {
		return fertilizationType;
	}

	public void setFertilizationType(String fertilizationType) {
		this.fertilizationType = fertilizationType;
	}

	public Integer getCarePlanId() {
		return carePlanId;
	}

	public void setCarePlanId(Integer carePlanId) {
		this.carePlanId = carePlanId;
	}

	public String getSperm() {
		return sperm;
	}

	public void setSperm(String sperm) {
		this.sperm = sperm;
	}

	public LocalDate getDay1() {
		return day1;
	}

	public void setDay1(LocalDate day1) {
		this.day1 = day1;
	}

	public Integer getDayTechNum() {
		return dayTechNum;
	}

	public void setDayTechNum(Integer dayTechNum) {
		this.dayTechNum = dayTechNum;
	}
	
}
