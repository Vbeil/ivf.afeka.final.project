package ivf.services.researchQuery;

import java.sql.SQLException;
import java.util.concurrent.ExecutionException;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface ResearchQueryService {

	/**
	 * Returns a JSON of the result set produced by the research query string
	 * 
	 * @param queryString The SQL query that will be performed in the DB
	 * @return Result set as JSON
	 * @throws SQLException
	 * @throws JsonProcessingException
	 * @throws ExecutionException
	 */
	public String customQuery(String queryString) throws SQLException, JsonProcessingException, ExecutionException;

}
