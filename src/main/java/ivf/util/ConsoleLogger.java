package ivf.util;

public class ConsoleLogger {

	private String messageOrigin;

	public ConsoleLogger(String messageOrigin) {
		this.messageOrigin = messageOrigin;
	}

	public void Print(String message, boolean important) {
		if (important)
			System.err.println(new String(String.format("%s | Message: %s", messageOrigin, message)));
		else
			System.out.println(new String(String.format("%s | Message: %s", messageOrigin, message)));
	}

	public void PrintSeparation() {
		System.err.println(ProjectConfiguration.LINE_SEPARATOR);
	}

}
