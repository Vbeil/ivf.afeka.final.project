package ivf.dataDeserializers;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class FloatDeserializer extends JsonDeserializer<Float> {

	@Override
	public Float deserialize(JsonParser parser, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {

		String doubleStr = parser.getText();

		if (doubleStr.isEmpty() || doubleStr == null || doubleStr.equalsIgnoreCase("NULL")) {
			return null;
		}

		return new Float(doubleStr);
	}
}