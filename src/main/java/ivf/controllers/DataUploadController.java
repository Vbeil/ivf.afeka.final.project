package ivf.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ivf.data.AccessEntity;
import ivf.data.BmiEntity;
import ivf.data.ComorbiditsEntity;
import ivf.data.DiagnosisEntity;
import ivf.data.FemaleMaleConnectionEntity;
import ivf.data.HabitsEntity;
import ivf.data.IVFHistoryEntity;
import ivf.data.MaleEntity;
import ivf.data.OPUDrugsEntity;
import ivf.data.OPUEntity;
import ivf.data.OtherTestsEntity;
import ivf.data.OvumFollowupEntity;
import ivf.data.PatientEntity;
import ivf.data.ProtocolsDrugsEntity;
import ivf.data.ProtocolsEntity;
import ivf.data.ProtocolsFollowupEntity;
import ivf.data.ProtocolsProceduresEntity;
import ivf.data.ProtocolsUSEntity;
import ivf.data.PuStorageEntity;
import ivf.data.ReturnActionDrugsEntity;
import ivf.data.ReturnActionEntity;
import ivf.data.SpermEntity;
import ivf.data.TreatmentSummaryEntity;
import ivf.data.UsPregnancyEntity;
import ivf.services.dataUpload.DataUploadService;
import ivf.util.ProjectConfiguration;

@RestController
public class DataUploadController {

	private DataUploadService dataUploadService;

	@Autowired
	public void setDataUploadService(DataUploadService dataUploadService) {
		this.dataUploadService = dataUploadService;
	}

	// Table #1 - Visits
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method = RequestMethod.POST, path = ProjectConfiguration.URL_DATA_UPLOAD_PATIENTS, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void uploadVisitsTable(@RequestBody PatientEntity[] patients, @PathVariable String override) {
		dataUploadService.uploadVisitsTable(patients, override);
	}

	// Table #2 - IVF History
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method = RequestMethod.POST, path = ProjectConfiguration.URL_DATA_UPLOAD_IVF_HISTORY, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void uploadIVFHistoryTable(@RequestBody IVFHistoryEntity[] ivfHistoryEntries,
			@PathVariable String override) {
		dataUploadService.uploadIVFHistoryTable(ivfHistoryEntries, override);
	}

	// Table #3 - Other Tests
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method = RequestMethod.POST, path = ProjectConfiguration.URL_DATA_UPLOAD_OTHER_TESTS, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void uploadOtherTestsTable(@RequestBody OtherTestsEntity[] otherTestsEntries,
			@PathVariable String override) {
		dataUploadService.uploadOtherTestsTable(otherTestsEntries, override);
	}

	// Table #4 - Diagnosis
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method = RequestMethod.POST, path = ProjectConfiguration.URL_DATA_UPLOAD_DIAGNOSIS, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void uploadDiagnosisTable(@RequestBody DiagnosisEntity[] diagnosisEntries, @PathVariable String override) {
		dataUploadService.uploadDiagnosisTable(diagnosisEntries, override);
	}

	// Table #5 - Protocols
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method = RequestMethod.POST, path = ProjectConfiguration.URL_DATA_UPLOAD_PROTOCOLS, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void uploadProtocolsTable(@RequestBody ProtocolsEntity[] protocolEntries, @PathVariable String override) {
		dataUploadService.uploadProtocolsTable(protocolEntries, override);
	}

	// Table #5.1 - Protocols Followup
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method = RequestMethod.POST, path = ProjectConfiguration.URL_DATA_UPLOAD_PROTOCOLS_FOLLOWUP, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void uploadProtocolsFollowupTable(@RequestBody ProtocolsFollowupEntity[] protocolFollowupEntries,
			@PathVariable String override) {
		dataUploadService.uploadProtocolsFollowupTable(protocolFollowupEntries, override);
	}

	// Table #5.2 - Protocols Procedures
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method = RequestMethod.POST, path = ProjectConfiguration.URL_DATA_UPLOAD_PROTOCOLS_PROCEDURES, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void uploadProtocolsProceduresTable(@RequestBody ProtocolsProceduresEntity[] protocolProceduresEntries,
			@PathVariable String override) {
		dataUploadService.uploadProtocolsProceduresTable(protocolProceduresEntries, override);
	}

	// Table #5.3 - Protocols US
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method = RequestMethod.POST, path = ProjectConfiguration.URL_DATA_UPLOAD_PROTOCOLS_US, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void uploadProtocolsUSTable(@RequestBody ProtocolsUSEntity[] protocolUsEntries,
			@PathVariable String override) {
		dataUploadService.uploadProtocolsUSTable(protocolUsEntries, override);
	}

	// Table #5.4 - Protocols Drugs
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method = RequestMethod.POST, path = ProjectConfiguration.URL_DATA_UPLOAD_PROTOCOLS_DRUGS, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void uploadProtocolDrugsTable(@RequestBody ProtocolsDrugsEntity[] protocolDrugsEntries,
			@PathVariable String override) {
		dataUploadService.uploadProtocolDrugsTable(protocolDrugsEntries, override);
	}

	// Table #6 - OPU
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method = RequestMethod.POST, path = ProjectConfiguration.URL_DATA_UPLOAD_OPU, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void uploadOPUTable(@RequestBody OPUEntity[] opuEntries, @PathVariable String override) {
		dataUploadService.uploadOPUTable(opuEntries, override);
	}

	// Table #6 - OPU Drugs
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method = RequestMethod.POST, path = ProjectConfiguration.URL_DATA_UPLOAD_OPU_DRUGS, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void uploadOPUDrugsTable(@RequestBody OPUDrugsEntity[] opuDrugsEntries, @PathVariable String override) {
		dataUploadService.uploadOPUDrugsTable(opuDrugsEntries, override);
	}

	// Table #7 - Return Action
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method = RequestMethod.POST, path = ProjectConfiguration.URL_DATA_UPLOAD_RETURN_ACTION, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void uploadReturnActionTable(@RequestBody ReturnActionEntity[] returnActionEntries,
			@PathVariable String override) {
		dataUploadService.uploadReturnActionTable(returnActionEntries, override);
	}

	// Table #7.1 - Return Action Drugs
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method = RequestMethod.POST, path = ProjectConfiguration.URL_DATA_UPLOAD_RETURN_ACTION_DRUGS, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void uploadReturnActionDrugsTable(@RequestBody ReturnActionDrugsEntity[] returnActionDrugsEntries,
			@PathVariable String override) {
		dataUploadService.uploadReturnActionDrugsTable(returnActionDrugsEntries, override);
	}

	// Table #8 - Sperm
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method = RequestMethod.POST, path = ProjectConfiguration.URL_DATA_UPLOAD_SPERM, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void uploadSpermTable(@RequestBody SpermEntity[] spermEntries, @PathVariable String override) {
		dataUploadService.uploadSpermTable(spermEntries, override);
	}

	// Table #9 - Ovum Followup
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method = RequestMethod.POST, path = ProjectConfiguration.URL_DATA_UPLOAD_OVUM_FOLLOWUP, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void uploadOvumFollowupTable(@RequestBody OvumFollowupEntity[] ovumFollowupEntries,
			@PathVariable String override) {
		dataUploadService.uploadOvumFollowupTable(ovumFollowupEntries, override);
	}

	// Table #10 - Pu Storage
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method = RequestMethod.POST, path = ProjectConfiguration.URL_DATA_UPLOAD_PU_STORAGE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void uploadPuStorageTable(@RequestBody PuStorageEntity[] puStorageEntries, @PathVariable String override) {
		dataUploadService.uploadPuStorageTable(puStorageEntries, override);
	}

	// Table #11 - Treatment Summary
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method = RequestMethod.POST, path = ProjectConfiguration.URL_DATA_UPLOAD_IVF_TREATMENT_SUMMARY, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void uploadTreatmentSummaryTable(@RequestBody TreatmentSummaryEntity[] treatmentSummaryEntries,
			@PathVariable String override) {
		dataUploadService.uploadTreatmentSummaryTable(treatmentSummaryEntries, override);
	}

	// Table #11.1 - UsPregnancy
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method = RequestMethod.POST, path = ProjectConfiguration.URL_DATA_UPLOAD_US_PREGNANCY, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void uploadUsPregnancyTable(@RequestBody UsPregnancyEntity[] usPregnancyEntries,
			@PathVariable String override) {
		dataUploadService.uploadUsPregnancyTable(usPregnancyEntries, override);
	}

	// Table #12 - Male
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method = RequestMethod.POST, path = ProjectConfiguration.URL_DATA_UPLOAD_MAN_IVF, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void uploadMaleTable(@RequestBody MaleEntity[] maleEntries, @PathVariable String override) {
		dataUploadService.uploadMaleTable(maleEntries, override);
	}

	// Table #13 - Comorbidits
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method = RequestMethod.POST, path = ProjectConfiguration.URL_DATA_UPLOAD_COMORBIDITS, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void uploadComorbiditsTable(@RequestBody ComorbiditsEntity[] comorbiditsEntries,
			@PathVariable String override) {
		dataUploadService.uploadComorbiditsTable(comorbiditsEntries, override);
	}

	// Table #14 - Habits
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method = RequestMethod.POST, path = ProjectConfiguration.URL_DATA_UPLOAD_HABITS, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void uploadHabitsTable(@RequestBody HabitsEntity[] habitsEntries, @PathVariable String override) {
		dataUploadService.uploadHabitsTable(habitsEntries, override);
	}

	// Table #15 - BMI
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method = RequestMethod.POST, path = ProjectConfiguration.URL_DATA_UPLOAD_BMI, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void uploadBmiTable(@RequestBody BmiEntity[] bmiEntries, @PathVariable String override) {
		dataUploadService.uploadBmiTable(bmiEntries, override);
	}

	// Table #16 - Access Data
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method = RequestMethod.POST, path = ProjectConfiguration.URL_DATA_UPLOAD_ACCESS, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void uploadAccessTable(@RequestBody AccessEntity[] accessPatients, @PathVariable String override) {
		dataUploadService.uploadAccessTable(accessPatients, override);
	}

	// Table #17 - Female Male Connection
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method = RequestMethod.POST, path = ProjectConfiguration.URL_DATA_UPLOAD_FEMALE_MALE_CONNECTION, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void uploadFemaleMaleConnectionTable(@RequestBody FemaleMaleConnectionEntity[] femaleMaleConnectionEntries,
			@PathVariable String override) {
		dataUploadService.uploadFemaleMaleConnectionTable(femaleMaleConnectionEntries, override);
	}

	// Clear Database and Cache
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method = RequestMethod.DELETE, path = ProjectConfiguration.URL_DATABASE_CLEAR)
	public void clearDatabaseAndCache() {
		dataUploadService.clearDatabaseAndCache();
	}

}
