package ivf.dao;

import org.springframework.data.repository.CrudRepository;

import ivf.data.FemaleMaleConnectionEntity;

public interface FemaleMaleConnectionDao extends CrudRepository<FemaleMaleConnectionEntity, Long> {

}
