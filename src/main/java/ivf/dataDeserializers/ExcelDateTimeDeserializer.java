package ivf.dataDeserializers;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import ivf.dataDeserializers.helpers.SerialDate;

public class ExcelDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {

	@Override
	public LocalDateTime deserialize(JsonParser parser, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {

		String dateTimeStr = parser.getText();

		if (dateTimeStr.isEmpty() || dateTimeStr == null || dateTimeStr.equalsIgnoreCase("NULL")) {
			return null;
		}

		SerialDate sd = new SerialDate(Double.parseDouble(dateTimeStr));
		LocalDateTime dt = LocalDateTime.of(LocalDate.ofEpochDay(sd.toEpochDays()),
				LocalTime.ofSecondOfDay(sd.toDaySeconds()));

		return dt;
	}

}
