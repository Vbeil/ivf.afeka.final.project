package ivf.data;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ivf.dataDeserializers.ExcelDateTimeDeserializer;
import ivf.dataDeserializers.IntegerDeserializer;
import ivf.dataDeserializers.StringNullCleanerDeserializer;
import ivf.dataDeserializers.YesNoToBooleanDeserializer;

// Table #2 - IVF History
@Entity
@Table(name = "IvfHistory")
public class IVFHistoryEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long rowId;

	@JsonProperty("idHash")
	private String patientId;

	@JsonDeserialize(using = ExcelDateTimeDeserializer.class)
	private LocalDateTime entryDate;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	@JsonProperty("admissionNoHash")
	private String admission;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer treatmentYear;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String protocol;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String fertilizationType;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer numberOfOvules;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer embryosOutcome;

	@JsonDeserialize(using = IntegerDeserializer.class)
	@JsonProperty("embryosFrozenE")
	private Integer embryosFrozen;

	@JsonDeserialize(using = IntegerDeserializer.class)
	@JsonProperty("embryoReturn")
	private Integer embryosReturned;

	@JsonDeserialize(using = YesNoToBooleanDeserializer.class)
	private Boolean pregnancy;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String endingKind;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String birthType;

	public IVFHistoryEntity() {

	}

	public IVFHistoryEntity(String patientId, LocalDateTime entryDate, String admission, Integer treatmentYear,
			String protocol, String fertilizationType, Integer numberOfOvules, Integer embryosOutcome,
			Integer embryosFrozenE, Integer embryosReturn, Boolean pregnancy, String endingKind, String birthType) {
		super();
		this.patientId = patientId;
		this.entryDate = entryDate;
		this.admission = admission;
		this.treatmentYear = treatmentYear;
		this.protocol = protocol;
		this.fertilizationType = fertilizationType;
		this.numberOfOvules = numberOfOvules;
		this.embryosOutcome = embryosOutcome;
		this.embryosFrozen = embryosFrozenE;
		this.embryosReturned = embryosReturn;
		this.pregnancy = pregnancy;
		this.endingKind = endingKind;
		this.birthType = birthType;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public LocalDateTime getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(LocalDateTime entryDate) {
		this.entryDate = entryDate;
	}

	public String getAdmission() {
		return admission;
	}

	public void setAdmission(String admission) {
		this.admission = admission;
	}

	public Integer getTreatmentYear() {
		return treatmentYear;
	}

	public void setTreatmentYear(Integer treatmentYear) {
		this.treatmentYear = treatmentYear;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getFertilizationType() {
		return fertilizationType;
	}

	public void setFertilizationType(String fertilizationType) {
		this.fertilizationType = fertilizationType;
	}

	public Integer getNumberOfOvules() {
		return numberOfOvules;
	}

	public void setNumberOfOvules(Integer numberOfOvules) {
		this.numberOfOvules = numberOfOvules;
	}

	public Integer getEmbryosOutcome() {
		return embryosOutcome;
	}

	public void setEmbryosOutcome(Integer embryosOutcome) {
		this.embryosOutcome = embryosOutcome;
	}

	public Integer getEmbryosFrozenE() {
		return embryosFrozen;
	}

	public void setEmbryosFrozenE(Integer embryosFrozenE) {
		this.embryosFrozen = embryosFrozenE;
	}

	public Integer getEmbryosReturn() {
		return embryosReturned;
	}

	public void setEmbryosReturn(Integer embryosReturn) {
		this.embryosReturned = embryosReturn;
	}

	public Boolean getPregnancy() {
		return pregnancy;
	}

	public void setPregnancy(Boolean pregnancy) {
		this.pregnancy = pregnancy;
	}

	public String getEndingKind() {
		return endingKind;
	}

	public void setEndingKind(String endingKind) {
		this.endingKind = endingKind;
	}

	public String getBirthType() {
		return birthType;
	}

	public void setBirthType(String birthType) {
		this.birthType = birthType;
	}

}
