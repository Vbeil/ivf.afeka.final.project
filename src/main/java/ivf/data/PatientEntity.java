package ivf.data;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ivf.dataDeserializers.ExcelDateDeserializer;
import ivf.dataDeserializers.ExcelDateTimeDeserializer;
import ivf.dataDeserializers.FloatDeserializer;
import ivf.dataDeserializers.IntegerDeserializer;
import ivf.dataDeserializers.StringNullCleanerDeserializer;
import ivf.dataDeserializers.YesNoToBooleanDeserializer;

// Table #1 - Visits
@Entity
@Table(name = "Patients")
public class PatientEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long rowId;

	@JsonProperty("idHash")
	private String patientId;

	@JsonDeserialize(using = ExcelDateTimeDeserializer.class)
	private LocalDateTime entryDate;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	@JsonProperty("admissionNoHash")
	private String admission;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String infertility;

	@JsonDeserialize(using = YesNoToBooleanDeserializer.class)
	private Boolean regularMenstruation;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String bleedingQuantity;

	@JsonDeserialize(using = YesNoToBooleanDeserializer.class)
	private Boolean pms;

	@JsonDeserialize(using = YesNoToBooleanDeserializer.class)
	private Boolean dyspareunia;

	@JsonDeserialize(using = YesNoToBooleanDeserializer.class)
	private Boolean galactorrhea;

	@JsonDeserialize(using = YesNoToBooleanDeserializer.class)
	private Boolean hirsutism;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String preventiveMeasures;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String hormonalProfile;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer infertilityDuration;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer firstMenstruationAge;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer menstruationDays;

	@JsonDeserialize(using = FloatDeserializer.class)
	private Float yearsOfMarriage;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	private LocalDate hormonalDate;

	// @JsonDeserialize(using = FloatDeserializer.class)
	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String tsh; // Problem

	// @JsonDeserialize(using = FloatDeserializer.class)
	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String fsh; // Problem

	// @JsonDeserialize(using = FloatDeserializer.class)
	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String lh;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String progesterone; // Problem

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String estradiol; // Problem

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String testosterone; // Problem

	@JsonDeserialize(using = FloatDeserializer.class)
	@JsonProperty("17OhProgesterone")
	private Float PROGESTERONE_17_OH;

	@JsonDeserialize(using = FloatDeserializer.class)
	private Float amh;

	@JsonDeserialize(using = FloatDeserializer.class)
	private Float dheas;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String prolactin; // Problem

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String diagnosis;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String treatmentReasons;

	public PatientEntity() {
	}

	public PatientEntity(Long rowId, String patientId, LocalDateTime entryDate, String admission, String infertility,
			Boolean regularMenstruation, String bleedingQuantity, Boolean pms, Boolean dyspareunia,
			Boolean galactorrhea, Boolean hirsutism, String preventiveMeasures, String hormonalProfile,
			Integer infertilityDuration, Integer firstMenstruationAge, Integer menstruationDays, Float yearsOfMarriage,
			LocalDate hormonalDate, String tsh, String fsh, String lh, String progesterone, String estradiol,
			String testosterone, Float pROGESTERONE_17_OH, Float amh, Float dheas, String prolactin, String diagnosis,
			String treatmentReasons) {
		super();
		this.rowId = rowId;
		this.patientId = patientId;
		this.entryDate = entryDate;
		this.admission = admission;
		this.infertility = infertility;
		this.regularMenstruation = regularMenstruation;
		this.bleedingQuantity = bleedingQuantity;
		this.pms = pms;
		this.dyspareunia = dyspareunia;
		this.galactorrhea = galactorrhea;
		this.hirsutism = hirsutism;
		this.preventiveMeasures = preventiveMeasures;
		this.hormonalProfile = hormonalProfile;
		this.infertilityDuration = infertilityDuration;
		this.firstMenstruationAge = firstMenstruationAge;
		this.menstruationDays = menstruationDays;
		this.yearsOfMarriage = yearsOfMarriage;
		this.hormonalDate = hormonalDate;
		this.tsh = tsh;
		this.fsh = fsh;
		this.lh = lh;
		this.progesterone = progesterone;
		this.estradiol = estradiol;
		this.testosterone = testosterone;
		PROGESTERONE_17_OH = pROGESTERONE_17_OH;
		this.amh = amh;
		this.dheas = dheas;
		this.prolactin = prolactin;
		this.diagnosis = diagnosis;
		this.treatmentReasons = treatmentReasons;
	}

	public Long getRowId() {
		return rowId;
	}

	public void setRowId(Long rowId) {
		this.rowId = rowId;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public LocalDateTime getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(LocalDateTime entryDate) {
		this.entryDate = entryDate;
	}

	public String getAdmission() {
		return admission;
	}

	public void setAdmission(String admission) {
		this.admission = admission;
	}

	public String getInfertility() {
		return infertility;
	}

	public void setInfertility(String infertility) {
		this.infertility = infertility;
	}

	public Boolean getRegularMenstruation() {
		return regularMenstruation;
	}

	public void setRegularMenstruation(Boolean regularMenstruation) {
		this.regularMenstruation = regularMenstruation;
	}

	public String getBleedingQuantity() {
		return bleedingQuantity;
	}

	public void setBleedingQuantity(String bleedingQuantity) {
		this.bleedingQuantity = bleedingQuantity;
	}

	public Boolean getPms() {
		return pms;
	}

	public void setPms(Boolean pms) {
		this.pms = pms;
	}

	public Boolean getDyspareunia() {
		return dyspareunia;
	}

	public void setDyspareunia(Boolean dyspareunia) {
		this.dyspareunia = dyspareunia;
	}

	public Boolean getGalactorrhea() {
		return galactorrhea;
	}

	public void setGalactorrhea(Boolean galactorrhea) {
		this.galactorrhea = galactorrhea;
	}

	public Boolean getHirsutism() {
		return hirsutism;
	}

	public void setHirsutism(Boolean hirsutism) {
		this.hirsutism = hirsutism;
	}

	public String getPreventiveMeasures() {
		return preventiveMeasures;
	}

	public void setPreventiveMeasures(String preventiveMeasures) {
		this.preventiveMeasures = preventiveMeasures;
	}

	public String getHormonalProfile() {
		return hormonalProfile;
	}

	public void setHormonalProfile(String hormonalProfile) {
		this.hormonalProfile = hormonalProfile;
	}


	public Integer getInfertilityDuration() {
		return infertilityDuration;
	}

	public void setInfertilityDuration(Integer infertilityDuration) {
		this.infertilityDuration = infertilityDuration;
	}

	public Integer getFirstMenstruationAge() {
		return firstMenstruationAge;
	}

	public void setFirstMenstruationAge(Integer firstMenstruationAge) {
		this.firstMenstruationAge = firstMenstruationAge;
	}

	public Integer getMenstruationDays() {
		return menstruationDays;
	}

	public void setMenstruationDays(Integer menstruationDays) {
		this.menstruationDays = menstruationDays;
	}

	public Float getYearsOfMarriage() {
		return yearsOfMarriage;
	}

	public void setYearsOfMarriage(Float yearsOfMarriage) {
		this.yearsOfMarriage = yearsOfMarriage;
	}

	public LocalDate getHormonalDate() {
		return hormonalDate;
	}

	public void setHormonalDate(LocalDate hormonalDate) {
		this.hormonalDate = hormonalDate;
	}

	public String getTsh() {
		return tsh;
	}

	public void setTsh(String tsh) {
		this.tsh = tsh;
	}

	public String getFsh() {
		return fsh;
	}

	public void setFsh(String fsh) {
		this.fsh = fsh;
	}

	public String getLh() {
		return lh;
	}

	public void setLh(String lh) {
		this.lh = lh;
	}

	public String getProgesterone() {
		return progesterone;
	}

	public void setProgesterone(String progesterone) {
		this.progesterone = progesterone;
	}

	public String getEstradiol() {
		return estradiol;
	}

	public void setEstradiol(String estradiol) {
		this.estradiol = estradiol;
	}

	public String getTestosterone() {
		return testosterone;
	}

	public void setTestosterone(String testosterone) {
		this.testosterone = testosterone;
	}

	public Float getPROGESTERONE_17_OH() {
		return PROGESTERONE_17_OH;
	}

	public void setPROGESTERONE_17_OH(Float pROGESTERONE_17_OH) {
		PROGESTERONE_17_OH = pROGESTERONE_17_OH;
	}

	public Float getAmh() {
		return amh;
	}

	public void setAmh(Float amh) {
		this.amh = amh;
	}

	public Float getDheas() {
		return dheas;
	}

	public void setDheas(Float dheas) {
		this.dheas = dheas;
	}

	public String getProlactin() {
		return prolactin;
	}

	public void setProlactin(String prolactin) {
		this.prolactin = prolactin;
	}

	public String getDiagnosis() {
		return diagnosis;
	}

	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}

	public String getTreatmentReasons() {
		return treatmentReasons;
	}

	public void setTreatmentReasons(String treatmentReasons) {
		this.treatmentReasons = treatmentReasons;
	}

}
