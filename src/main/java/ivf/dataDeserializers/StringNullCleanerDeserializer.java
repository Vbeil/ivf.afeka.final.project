package ivf.dataDeserializers;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class StringNullCleanerDeserializer extends JsonDeserializer<String> {

	@Override
	public String deserialize(JsonParser parser, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {

		String str = parser.getText();

		if (str.isEmpty() || str == null || str.equalsIgnoreCase("NULL") || str.equalsIgnoreCase("NA")) {
			return null;
		}

		return new String(str);
	}

}
