package ivf.dataDeserializers;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class YesNoToBooleanDeserializer extends JsonDeserializer<Boolean> {

	@Override
	public Boolean deserialize(JsonParser parser, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {

		String yesNoStr = parser.getText();

		if (yesNoStr.isEmpty() || yesNoStr == null || yesNoStr.equalsIgnoreCase("NULL")
				|| yesNoStr.equalsIgnoreCase("NA")) {
			return null;
		}

		if (yesNoStr.equalsIgnoreCase("Yes") || yesNoStr.equalsIgnoreCase("כן") || yesNoStr.equalsIgnoreCase("1"))
			return true;
		else if (yesNoStr.equalsIgnoreCase("No") || yesNoStr.equalsIgnoreCase("לא") || yesNoStr.equalsIgnoreCase("2")
				|| yesNoStr.equalsIgnoreCase("0"))
			return false;
		else
			return null;
	}
}