package ivf.dao;

import org.springframework.data.repository.CrudRepository;

import ivf.data.ProtocolsProceduresEntity;

public interface ProtocolsProceduresDao extends CrudRepository<ProtocolsProceduresEntity, Long> {

}
