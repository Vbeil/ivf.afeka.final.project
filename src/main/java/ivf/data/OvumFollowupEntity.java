package ivf.data;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ivf.dataDeserializers.ExcelDateDeserializer;
import ivf.dataDeserializers.FloatDeserializer;
import ivf.dataDeserializers.IntegerDeserializer;
import ivf.dataDeserializers.StringNullCleanerDeserializer;
import ivf.dataDeserializers.YesNoToBooleanDeserializer;

@Entity
@Table(name = "OvumFollowup")
public class OvumFollowupEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long rowId;

	@JsonProperty("idHash")
	private String patientId;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	private LocalDate pickUpDate;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer pickUpId;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer ovumId;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String finalStatus;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer finalStatusDtNo;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String ivfMethodD0;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String maturityDegreeD0;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String mediumD0;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String ivfResultD1;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String cellCountD1;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String cellCountD2; // Problem

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer qualityDegreeD2;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String cellCountD3; // Problem

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer qualityDegreeD3;

	@JsonDeserialize(using = FloatDeserializer.class)
	private Float embryoscopeGradeD3;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String pdgTypeD3;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer sampleCellsD3;

	@JsonDeserialize(using = YesNoToBooleanDeserializer.class)
	private Boolean cellulaD3;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String assistedHatchingD3; // Problem - All NULLS

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String cellCountD4; // Problem

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer qualityDegreeD4;

	@JsonDeserialize(using = FloatDeserializer.class)
	private Float embryoscopeGradeD4;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer sampleCellsD4;

	@JsonDeserialize(using = YesNoToBooleanDeserializer.class)
	private Boolean cellulaD4;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String assistedHatchingD4; // Problem - All NULLS

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String pdgAnswerD4;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer qualityDegreeD5;

	@JsonDeserialize(using = FloatDeserializer.class)
	private Float embryoscopeGradeD5;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String pdgTypeD5;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String sampleCellsD5;

	@JsonDeserialize(using = YesNoToBooleanDeserializer.class)
	private Boolean cellulaD5;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String assistedHatchingD5; // Problem - All NULLS

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String pdgAnswerD5;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer genetigLabRecomandionD5; // Problem - Should it be geneticLabRecommendationD5?

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String bltD5;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String bltQualityD5;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer qualityDegreeD6;

	@JsonDeserialize(using = FloatDeserializer.class)
	private Float embryoscopeGradeD6;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String pdgTypeD6;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String sampleCellsD6;

	@JsonDeserialize(using = YesNoToBooleanDeserializer.class)
	private Boolean cellulaD6;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String assistedHatchingD6; // Problem - All NULLS

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String pdgAnswerD6;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer genetigLabRecomandionD6; // Problem - Should it be geneticLabRecommendationD6?

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String bltD6;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String bltQualityD6;

	public OvumFollowupEntity() {
	}

	public OvumFollowupEntity(String patientId, LocalDate pickupDate, Integer pickupId, Integer ovumId,
			String finalStatus, Integer finalStatusDtNo, String ivfMethodD0, String maturityDegreeD0, String mediumD0,
			String ivfResultD1, String cellCountD1, String cellCountD2, Integer qualityDegreeD2, String cellCountD3,
			Integer qualityDegreeD3, Float embryoscopeGradeD3, String pdgTypeD3, Integer sampleCellsD3,
			Boolean cellulaD3, String assistedHatchingD3, String cellCountD4, Integer qualityDegreeD4,
			Float embryoscopeGradeD4, Integer sampleCellsD4, Boolean cellulaD4, String assistedHatchingD4,
			String pdgAnswerD4, Integer qualityDegreeD5, Float embryoscopeGradeD5, String pdgTypeD5,
			String sampleCellsD5, Boolean cellulaD5, String assistedHatchingD5, String pdgAnswerD5,
			Integer genetigLabRecomandionD5, String bltD5, String bltQualityD5, Integer qualityDegreeD6,
			Float embryoscopeGradeD6, String pdgTypeD6, String sampleCellsD6, Boolean cellulaD6,
			String assistedHatchingD6, String pdgAnswerD6, Integer genetigLabRecomandionD6, String bltD6,
			String bltQualityD6) {
		super();
		this.patientId = patientId;
		this.pickUpDate = pickupDate;
		this.pickUpId = pickupId;
		this.ovumId = ovumId;
		this.finalStatus = finalStatus;
		this.finalStatusDtNo = finalStatusDtNo;
		this.ivfMethodD0 = ivfMethodD0;
		this.maturityDegreeD0 = maturityDegreeD0;
		this.mediumD0 = mediumD0;
		this.ivfResultD1 = ivfResultD1;
		this.cellCountD1 = cellCountD1;
		this.cellCountD2 = cellCountD2;
		this.qualityDegreeD2 = qualityDegreeD2;
		this.cellCountD3 = cellCountD3;
		this.qualityDegreeD3 = qualityDegreeD3;
		this.embryoscopeGradeD3 = embryoscopeGradeD3;
		this.pdgTypeD3 = pdgTypeD3;
		this.sampleCellsD3 = sampleCellsD3;
		this.cellulaD3 = cellulaD3;
		this.assistedHatchingD3 = assistedHatchingD3;
		this.cellCountD4 = cellCountD4;
		this.qualityDegreeD4 = qualityDegreeD4;
		this.embryoscopeGradeD4 = embryoscopeGradeD4;
		this.sampleCellsD4 = sampleCellsD4;
		this.cellulaD4 = cellulaD4;
		this.assistedHatchingD4 = assistedHatchingD4;
		this.pdgAnswerD4 = pdgAnswerD4;
		this.qualityDegreeD5 = qualityDegreeD5;
		this.embryoscopeGradeD5 = embryoscopeGradeD5;
		this.pdgTypeD5 = pdgTypeD5;
		this.sampleCellsD5 = sampleCellsD5;
		this.cellulaD5 = cellulaD5;
		this.assistedHatchingD5 = assistedHatchingD5;
		this.pdgAnswerD5 = pdgAnswerD5;
		this.genetigLabRecomandionD5 = genetigLabRecomandionD5;
		this.bltD5 = bltD5;
		this.bltQualityD5 = bltQualityD5;
		this.qualityDegreeD6 = qualityDegreeD6;
		this.embryoscopeGradeD6 = embryoscopeGradeD6;
		this.pdgTypeD6 = pdgTypeD6;
		this.sampleCellsD6 = sampleCellsD6;
		this.cellulaD6 = cellulaD6;
		this.assistedHatchingD6 = assistedHatchingD6;
		this.pdgAnswerD6 = pdgAnswerD6;
		this.genetigLabRecomandionD6 = genetigLabRecomandionD6;
		this.bltD6 = bltD6;
		this.bltQualityD6 = bltQualityD6;
	}

	public Long getRowId() {
		return rowId;
	}

	public void setRowId(Long rowId) {
		this.rowId = rowId;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public LocalDate getPickupDate() {
		return pickUpDate;
	}

	public void setPickupDate(LocalDate pickupDate) {
		this.pickUpDate = pickupDate;
	}

	public Integer getPickupId() {
		return pickUpId;
	}

	public void setPickupId(Integer pickupId) {
		this.pickUpId = pickupId;
	}

	public Integer getOvumId() {
		return ovumId;
	}

	public void setOvumId(Integer ovumId) {
		this.ovumId = ovumId;
	}

	public String getFinalStatus() {
		return finalStatus;
	}

	public void setFinalStatus(String finalStatus) {
		this.finalStatus = finalStatus;
	}

	public Integer getFinalStatusDtNo() {
		return finalStatusDtNo;
	}

	public void setFinalStatusDtNo(Integer finalStatusDtNo) {
		this.finalStatusDtNo = finalStatusDtNo;
	}

	public String getIvfMethodD0() {
		return ivfMethodD0;
	}

	public void setIvfMethodD0(String ivfMethodD0) {
		this.ivfMethodD0 = ivfMethodD0;
	}

	public String getMaturityDegreeD0() {
		return maturityDegreeD0;
	}

	public void setMaturityDegreeD0(String maturityDegreeD0) {
		this.maturityDegreeD0 = maturityDegreeD0;
	}

	public String getMediumD0() {
		return mediumD0;
	}

	public void setMediumD0(String mediumD0) {
		this.mediumD0 = mediumD0;
	}

	public String getIvfResultD1() {
		return ivfResultD1;
	}

	public void setIvfResultD1(String ivfResultD1) {
		this.ivfResultD1 = ivfResultD1;
	}

	public String getCellCountD1() {
		return cellCountD1;
	}

	public void setCellCountD1(String cellCountD1) {
		this.cellCountD1 = cellCountD1;
	}

	public String getCellCountD2() {
		return cellCountD2;
	}

	public void setCellCountD2(String cellCountD2) {
		this.cellCountD2 = cellCountD2;
	}

	public Integer getQualityDegreeD2() {
		return qualityDegreeD2;
	}

	public void setQualityDegreeD2(Integer qualityDegreeD2) {
		this.qualityDegreeD2 = qualityDegreeD2;
	}

	public String getCellCountD3() {
		return cellCountD3;
	}

	public void setCellCountD3(String cellCountD3) {
		this.cellCountD3 = cellCountD3;
	}

	public Integer getQualityDegreeD3() {
		return qualityDegreeD3;
	}

	public void setQualityDegreeD3(Integer qualityDegreeD3) {
		this.qualityDegreeD3 = qualityDegreeD3;
	}

	public Float getEmbryoscopeGradeD3() {
		return embryoscopeGradeD3;
	}

	public void setEmbryoscopeGradeD3(Float embryoscopeGradeD3) {
		this.embryoscopeGradeD3 = embryoscopeGradeD3;
	}

	public String getPdgTypeD3() {
		return pdgTypeD3;
	}

	public void setPdgTypeD3(String pdgTypeD3) {
		this.pdgTypeD3 = pdgTypeD3;
	}

	public Integer getSampleCellsD3() {
		return sampleCellsD3;
	}

	public void setSampleCellsD3(Integer sampleCellsD3) {
		this.sampleCellsD3 = sampleCellsD3;
	}

	public Boolean getCellulaD3() {
		return cellulaD3;
	}

	public void setCellulaD3(Boolean cellulaD3) {
		this.cellulaD3 = cellulaD3;
	}

	public String getAssistedHatchingD3() {
		return assistedHatchingD3;
	}

	public void setAssistedHatchingD3(String assistedHatchingD3) {
		this.assistedHatchingD3 = assistedHatchingD3;
	}

	public String getCellCountD4() {
		return cellCountD4;
	}

	public void setCellCountD4(String cellCountD4) {
		this.cellCountD4 = cellCountD4;
	}

	public Integer getQualityDegreeD4() {
		return qualityDegreeD4;
	}

	public void setQualityDegreeD4(Integer qualityDegreeD4) {
		this.qualityDegreeD4 = qualityDegreeD4;
	}

	public Float getEmbryoscopeGradeD4() {
		return embryoscopeGradeD4;
	}

	public void setEmbryoscopeGradeD4(Float embryoscopeGradeD4) {
		this.embryoscopeGradeD4 = embryoscopeGradeD4;
	}

	public Integer getSampleCellsD4() {
		return sampleCellsD4;
	}

	public void setSampleCellsD4(Integer sampleCellsD4) {
		this.sampleCellsD4 = sampleCellsD4;
	}

	public Boolean getCellulaD4() {
		return cellulaD4;
	}

	public void setCellulaD4(Boolean cellulaD4) {
		this.cellulaD4 = cellulaD4;
	}

	public String getAssistedHatchingD4() {
		return assistedHatchingD4;
	}

	public void setAssistedHatchingD4(String assistedHatchingD4) {
		this.assistedHatchingD4 = assistedHatchingD4;
	}

	public String getPdgAnswerD4() {
		return pdgAnswerD4;
	}

	public void setPdgAnswerD4(String pdgAnswerD4) {
		this.pdgAnswerD4 = pdgAnswerD4;
	}

	public Integer getQualityDegreeD5() {
		return qualityDegreeD5;
	}

	public void setQualityDegreeD5(Integer qualityDegreeD5) {
		this.qualityDegreeD5 = qualityDegreeD5;
	}

	public Float getEmbryoscopeGradeD5() {
		return embryoscopeGradeD5;
	}

	public void setEmbryoscopeGradeD5(Float embryoscopeGradeD5) {
		this.embryoscopeGradeD5 = embryoscopeGradeD5;
	}

	public String getPdgTypeD5() {
		return pdgTypeD5;
	}

	public void setPdgTypeD5(String pdgTypeD5) {
		this.pdgTypeD5 = pdgTypeD5;
	}

	public String getSampleCellsD5() {
		return sampleCellsD5;
	}

	public void setSampleCellsD5(String sampleCellsD5) {
		this.sampleCellsD5 = sampleCellsD5;
	}

	public Boolean getCellulaD5() {
		return cellulaD5;
	}

	public void setCellulaD5(Boolean cellulaD5) {
		this.cellulaD5 = cellulaD5;
	}

	public String getAssistedHatchingD5() {
		return assistedHatchingD5;
	}

	public void setAssistedHatchingD5(String assistedHatchingD5) {
		this.assistedHatchingD5 = assistedHatchingD5;
	}

	public String getPdgAnswerD5() {
		return pdgAnswerD5;
	}

	public void setPdgAnswerD5(String pdgAnswerD5) {
		this.pdgAnswerD5 = pdgAnswerD5;
	}

	public Integer getGenetigLabRecomandionD5() {
		return genetigLabRecomandionD5;
	}

	public void setGenetigLabRecomandionD5(Integer genetigLabRecomandionD5) {
		this.genetigLabRecomandionD5 = genetigLabRecomandionD5;
	}

	public String getBltD5() {
		return bltD5;
	}

	public void setBltD5(String bltD5) {
		this.bltD5 = bltD5;
	}

	public String getBltQualityD5() {
		return bltQualityD5;
	}

	public void setBltQualityD5(String bltQualityD5) {
		this.bltQualityD5 = bltQualityD5;
	}

	public Integer getQualityDegreeD6() {
		return qualityDegreeD6;
	}

	public void setQualityDegreeD6(Integer qualityDegreeD6) {
		this.qualityDegreeD6 = qualityDegreeD6;
	}

	public Float getEmbryoscopeGradeD6() {
		return embryoscopeGradeD6;
	}

	public void setEmbryoscopeGradeD6(Float embryoscopeGradeD6) {
		this.embryoscopeGradeD6 = embryoscopeGradeD6;
	}

	public String getPdgTypeD6() {
		return pdgTypeD6;
	}

	public void setPdgTypeD6(String pdgTypeD6) {
		this.pdgTypeD6 = pdgTypeD6;
	}

	public String getSampleCellsD6() {
		return sampleCellsD6;
	}

	public void setSampleCellsD6(String sampleCellsD6) {
		this.sampleCellsD6 = sampleCellsD6;
	}

	public Boolean getCellulaD6() {
		return cellulaD6;
	}

	public void setCellulaD6(Boolean cellulaD6) {
		this.cellulaD6 = cellulaD6;
	}

	public String getAssistedHatchingD6() {
		return assistedHatchingD6;
	}

	public void setAssistedHatchingD6(String assistedHatchingD6) {
		this.assistedHatchingD6 = assistedHatchingD6;
	}

	public String getPdgAnswerD6() {
		return pdgAnswerD6;
	}

	public void setPdgAnswerD6(String pdgAnswerD6) {
		this.pdgAnswerD6 = pdgAnswerD6;
	}

	public Integer getGenetigLabRecomandionD6() {
		return genetigLabRecomandionD6;
	}

	public void setGenetigLabRecomandionD6(Integer genetigLabRecomandionD6) {
		this.genetigLabRecomandionD6 = genetigLabRecomandionD6;
	}

	public String getBltD6() {
		return bltD6;
	}

	public void setBltD6(String bltD6) {
		this.bltD6 = bltD6;
	}

	public String getBltQualityD6() {
		return bltQualityD6;
	}

	public void setBltQualityD6(String bltQualityD6) {
		this.bltQualityD6 = bltQualityD6;
	}

}
