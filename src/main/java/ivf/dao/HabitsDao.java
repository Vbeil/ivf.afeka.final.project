package ivf.dao;

import org.springframework.data.repository.CrudRepository;

import ivf.data.HabitsEntity;

public interface HabitsDao extends CrudRepository<HabitsEntity, Long> {

}
