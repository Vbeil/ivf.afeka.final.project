package ivf.dao;


import org.springframework.data.repository.CrudRepository;

import ivf.data.IVFHistoryEntity;

public interface IVFHistoryDao extends CrudRepository<IVFHistoryEntity, Long> {

}
