package ivf.data;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ivf.dataDeserializers.ExcelDateTimeDeserializer;
import ivf.dataDeserializers.FloatDeserializer;
import ivf.dataDeserializers.IntegerDeserializer;
import ivf.dataDeserializers.StringNullCleanerDeserializer;

@Entity
@Table(name = "Sperm")
public class SpermEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long rowId;

	@JsonProperty("idHash")
	private String patientId;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	@JsonProperty("mIdHash")
	private String patientPartnerId;

	@JsonDeserialize(using = ExcelDateTimeDeserializer.class)
	private LocalDateTime returnActionDate;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String origin;

	@JsonDeserialize(using = FloatDeserializer.class)
	private Float volume;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer movements;

	@JsonDeserialize(using = FloatDeserializer.class)
	private Float countBefore;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer morphology;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer examType;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String examTypeText;

	public SpermEntity() {

	}

	public SpermEntity(String patientId, String patientPartnerId, LocalDateTime returnActionDate, String origin,
			Float volume, Integer movements, Float countBefore, Integer morphology, Integer examType,
			String examTypeText) {
		super();
		this.patientId = patientId;
		this.patientPartnerId = patientPartnerId;
		this.returnActionDate = returnActionDate;
		this.origin = origin;
		this.volume = volume;
		this.movements = movements;
		this.countBefore = countBefore;
		this.morphology = morphology;
		this.examType = examType;
		this.examTypeText = examTypeText;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public String getPatientPartnerId() {
		return patientPartnerId;
	}

	public void setPatientPartnerId(String patientPartnerId) {
		this.patientPartnerId = patientPartnerId;
	}

	public LocalDateTime getReturnActionDate() {
		return returnActionDate;
	}

	public void setReturnActionDate(LocalDateTime returnActionDate) {
		this.returnActionDate = returnActionDate;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public Float getVolume() {
		return volume;
	}

	public void setVolume(Float volume) {
		this.volume = volume;
	}

	public Integer getMovements() {
		return movements;
	}

	public void setMovements(Integer movements) {
		this.movements = movements;
	}

	public Float getCountBefore() {
		return countBefore;
	}

	public void setCountBefore(Float countBefore) {
		this.countBefore = countBefore;
	}

	public Integer getMorphology() {
		return morphology;
	}

	public void setMorphology(Integer morphology) {
		this.morphology = morphology;
	}

	public Integer getExamType() {
		return examType;
	}

	public void setExamType(Integer examType) {
		this.examType = examType;
	}

	public String getExamTypeText() {
		return examTypeText;
	}

	public void setExamTypeText(String examTypeText) {
		this.examTypeText = examTypeText;
	}


}
