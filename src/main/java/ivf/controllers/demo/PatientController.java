package ivf.controllers.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ivf.data.PatientEntity;
import ivf.services.tables.PatientsService;

@RestController
public class PatientController {
	private PatientsService patientsHandler;

	@Autowired
	public void setPatientsHandler(PatientsService patientsHandler) {
		this.patientsHandler = patientsHandler;
	}

	// get specific patient
	@RequestMapping(method = RequestMethod.GET, path = "/ivf/patients/{patientId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public PatientEntity patient(@PathVariable("patientId") Long patientId) throws Exception {
		return patientsHandler.getSpecificPatient(patientId);
	}

	// get all patients
	@RequestMapping(method = RequestMethod.GET, path = "/ivf/patients/", produces = MediaType.APPLICATION_JSON_VALUE)
	public PatientEntity[] patients() {
		return patientsHandler.getAll().toArray(new PatientEntity[0]);
	}

	// update specific patient
	@RequestMapping(method = RequestMethod.PUT, path = "/ivf/patients/{patientId}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public PatientEntity updateExistingPatient(
			@PathVariable("patientId") Long patientId, @RequestBody PatientEntity updatedPatient) throws Exception {
		return patientsHandler.update(patientId, updatedPatient);
	}

	// create a patient
	@RequestMapping(path = "/ivf/patients/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public PatientEntity storePatient(@RequestBody PatientEntity newPatient) throws Exception {
		return patientsHandler.create(newPatient);
	}

	// delete all patients
	@RequestMapping(method = RequestMethod.DELETE, path = "/ivf/patients/")
	public void clear() {
		patientsHandler.deleteAll();
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method = RequestMethod.POST, path = "/ivf/patients/", produces = MediaType.APPLICATION_JSON_VALUE)
	public PatientEntity[] createMany(@RequestBody PatientEntity[] patients) throws Exception {
		return patientsHandler.create(patients);
	}
}
