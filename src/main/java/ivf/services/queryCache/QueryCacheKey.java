package ivf.services.queryCache;

import java.util.Arrays;

public class QueryCacheKey {

	private String sqlQuery;
	private String sortedQuerySelectFilters;
	private String[] sortedQueryWhereFilters;

	public QueryCacheKey(String sqlQuery) {
		this.sqlQuery = sqlQuery;
		seperateQuery(sqlQuery);
	}

	private void seperateQuery(String sqlQuery) {

		String[] queryParts = sqlQuery.split("WHERE", 2);
		this.sortedQuerySelectFilters = queryParts[0];
		String sortedQueryWhereFiltersString = queryParts[1];

		sortedQueryWhereFilters = sortedQueryWhereFiltersString.split("(?=AND|OR|NOT)");

		for (int i = 0; i < sortedQueryWhereFilters.length; i++)
			sortedQueryWhereFilters[i] = sortedQueryWhereFilters[i].replaceAll("\\s+", "");

		Arrays.sort(sortedQueryWhereFilters);

	}

	public String getSqlQuery() {
		return sqlQuery;
	}

	public void setSqlQuery(String sqlQuery) {
		this.sqlQuery = sqlQuery;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((sortedQuerySelectFilters == null) ? 0 : sortedQuerySelectFilters.hashCode());
		result = prime * result + Arrays.hashCode(sortedQueryWhereFilters);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		QueryCacheKey other = (QueryCacheKey) obj;
		if (sortedQuerySelectFilters == null) {
			if (other.sortedQuerySelectFilters != null)
				return false;
		} else if (!sortedQuerySelectFilters.equals(other.sortedQuerySelectFilters))
			return false;
		if (!Arrays.equals(sortedQueryWhereFilters, other.sortedQueryWhereFilters))
			return false;
		return true;
	}

}
