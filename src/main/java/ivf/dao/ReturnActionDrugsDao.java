package ivf.dao;

import org.springframework.data.repository.CrudRepository;

import ivf.data.ReturnActionDrugsEntity;

public interface ReturnActionDrugsDao extends CrudRepository<ReturnActionDrugsEntity, Long> {

}
