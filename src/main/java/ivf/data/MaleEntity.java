package ivf.data;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ivf.dataDeserializers.ExcelDateDeserializer;
import ivf.dataDeserializers.ExcelDateTimeDeserializer;
import ivf.dataDeserializers.IntegerDeserializer;
import ivf.dataDeserializers.StringNullCleanerDeserializer;
import ivf.dataDeserializers.YesNoToBooleanDeserializer;

@Entity
@Table(name = "Male")
public class MaleEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long rowId;

	@JsonProperty("idHashMen")
	private String malePatientId;

	@JsonDeserialize(using = ExcelDateTimeDeserializer.class)
	private LocalDateTime entryDate;

	@JsonDeserialize(using = IntegerDeserializer.class)
	@JsonProperty("relationToPatient")
	private Integer relationToPatientCode;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String relationToPatientText;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer marriageCount;

	@JsonDeserialize(using = IntegerDeserializer.class)
	@JsonProperty("childsFromCurrentMarriage")
	private Integer childrenFromCurrentMarriage;

	@JsonDeserialize(using = IntegerDeserializer.class)
	@JsonProperty("childsFromPreviusMarriage")
	private Integer childrenFromPreviousMarriage;

	@JsonDeserialize(using = YesNoToBooleanDeserializer.class)
	private Boolean isFirstMarriage;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer ctsTestType;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String ctsTestTypeText;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	private LocalDate ctsTestDate;

	@JsonDeserialize(using = IntegerDeserializer.class)
	@JsonProperty("ctsTestResult")
	private Integer ctsTestResultCode;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String ctsTestResultText;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	private LocalDate ctsTest2Date; // Problem - All NULLS

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String diagnosis1;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	@JsonProperty("diagnosisdate1")
	private LocalDate diagnosisDate1;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String diagnosis2;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	@JsonProperty("diagnosisdate2")
	private LocalDate diagnosisDate2;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	private LocalDate checkDate;

	@JsonDeserialize(using = IntegerDeserializer.class)
	@JsonProperty("rTesticlePos")
	private Integer rightTesticlePosition;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	@JsonProperty("rTesticlePosText")
	private String rightTesticlePositionText;

	@JsonDeserialize(using = IntegerDeserializer.class)
	@JsonProperty("rTesticleSize")
	private Integer rightTesticleSize;

	@JsonDeserialize(using = IntegerDeserializer.class)
	@JsonProperty("rConistenitzia")
	private Integer rightConistenitzia;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	@JsonProperty("rConistenitziaText")
	private String rightConistenitziaText;

	@JsonDeserialize(using = IntegerDeserializer.class)
	@JsonProperty("rVaricocele")
	private Integer rightVaricocele;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	@JsonProperty("rVaricoceleText")
	private String rightVaricoceleText;

	@JsonDeserialize(using = IntegerDeserializer.class)
	@JsonProperty("rHydrocele")
	private Integer rightHydrocele;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	@JsonProperty("rHydroceleText")
	private String rightHydroceleText;

	@JsonDeserialize(using = IntegerDeserializer.class)
	@JsonProperty("rHernia")
	private Integer rightHernia;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	@JsonProperty("rHerniaText")
	private String rightHerniaText;

	@JsonDeserialize(using = IntegerDeserializer.class)
	@JsonProperty("lTesticlePos")
	private Integer leftTesticlePosition;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	@JsonProperty("lTesticlePosText")
	private String leftTesticlePositionText;

	@JsonDeserialize(using = IntegerDeserializer.class)
	@JsonProperty("lTesticleSize")
	private Integer leftTesticleSize;

	@JsonDeserialize(using = IntegerDeserializer.class)
	@JsonProperty("lConistenitzia")
	private Integer leftConistenitzia;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	@JsonProperty("lConistenitziaText")
	private String leftConistenitziaText;

	@JsonDeserialize(using = IntegerDeserializer.class)
	@JsonProperty("lVaricocele")
	private Integer leftVaricocele;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	@JsonProperty("lVaricoceleText")
	private String leftVaricoceleText;

	@JsonDeserialize(using = IntegerDeserializer.class)
	@JsonProperty("lHydrocele")
	private Integer leftHydrocele;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	@JsonProperty("lHydroceleText")
	private String leftHydroceleText;

	@JsonDeserialize(using = IntegerDeserializer.class)
	@JsonProperty("lHernia")
	private Integer leftHernia;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	@JsonProperty("lHerniaText")
	private String leftHerniaText;

	public MaleEntity() {

	}

	public MaleEntity(String malePatientId, LocalDateTime entryDate, Integer relationToPatientCode,
			String relationToPatientText, Integer marriageCount, Integer childrenFromCurrentMarriage,
			Integer childrenFromPreviousMarriage, Boolean isFirstMarriage, Integer ctsTestType, String ctsTestTypeText,
			LocalDate ctsTestDate, Integer ctsTestResultCode, String ctsTestResultText, LocalDate ctsTest2Date,
			String diagnosis1, LocalDate diagnosisDate1,
			String diagnosis2, LocalDate diagnosisDate2, LocalDate checkDate, Integer rightTesticlePosition,
			String rightTesticlePositionText, Integer rightTesticleSize, Integer rightConistenitzia,
			String rightConistenitziaText, Integer rightVaricocele, String rightVaricoceleText, Integer rightHydrocele,
			String rightHydroceleText, Integer rightHernia, String rightHerniaText, Integer leftTesticlePosition,
			String leftTesticlePositionText, Integer leftTesticleSize, Integer leftConistenitzia,
			String leftConistenitziaText, Integer leftVaricocele, String leftVaricoceleText, Integer leftHydrocele,
			String leftHydroceleText, Integer leftHernia, String leftHerniaText) {
		super();
		this.malePatientId = malePatientId;
		this.entryDate = entryDate;
		this.relationToPatientCode = relationToPatientCode;
		this.relationToPatientText = relationToPatientText;
		this.marriageCount = marriageCount;
		this.childrenFromCurrentMarriage = childrenFromCurrentMarriage;
		this.childrenFromPreviousMarriage = childrenFromPreviousMarriage;
		this.isFirstMarriage = isFirstMarriage;
		this.ctsTestType = ctsTestType;
		this.ctsTestTypeText = ctsTestTypeText;
		this.ctsTestDate = ctsTestDate;
		this.ctsTestResultCode = ctsTestResultCode;
		this.ctsTestResultText = ctsTestResultText;
		this.ctsTest2Date = ctsTest2Date;
		this.diagnosis1 = diagnosis1;
		this.diagnosisDate1 = diagnosisDate1;
		this.diagnosis2 = diagnosis2;
		this.diagnosisDate2 = diagnosisDate2;
		this.checkDate = checkDate;
		this.rightTesticlePosition = rightTesticlePosition;
		this.rightTesticlePositionText = rightTesticlePositionText;
		this.rightTesticleSize = rightTesticleSize;
		this.rightConistenitzia = rightConistenitzia;
		this.rightConistenitziaText = rightConistenitziaText;
		this.rightVaricocele = rightVaricocele;
		this.rightVaricoceleText = rightVaricoceleText;
		this.rightHydrocele = rightHydrocele;
		this.rightHydroceleText = rightHydroceleText;
		this.rightHernia = rightHernia;
		this.rightHerniaText = rightHerniaText;
		this.leftTesticlePosition = leftTesticlePosition;
		this.leftTesticlePositionText = leftTesticlePositionText;
		this.leftTesticleSize = leftTesticleSize;
		this.leftConistenitzia = leftConistenitzia;
		this.leftConistenitziaText = leftConistenitziaText;
		this.leftVaricocele = leftVaricocele;
		this.leftVaricoceleText = leftVaricoceleText;
		this.leftHydrocele = leftHydrocele;
		this.leftHydroceleText = leftHydroceleText;
		this.leftHernia = leftHernia;
		this.leftHerniaText = leftHerniaText;
	}

	public String getMalePatientId() {
		return malePatientId;
	}

	public void setMalePatientId(String malePatientId) {
		this.malePatientId = malePatientId;
	}

	public LocalDateTime getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(LocalDateTime entryDate) {
		this.entryDate = entryDate;
	}

	public Integer getRelationToPatientCode() {
		return relationToPatientCode;
	}

	public void setRelationToPatientCode(Integer relationToPatientCode) {
		this.relationToPatientCode = relationToPatientCode;
	}

	public String getRelationToPatientText() {
		return relationToPatientText;
	}

	public void setRelationToPatientText(String relationToPatientText) {
		this.relationToPatientText = relationToPatientText;
	}

	public Integer getMarriageCount() {
		return marriageCount;
	}

	public void setMarriageCount(Integer marriageCount) {
		this.marriageCount = marriageCount;
	}

	public Integer getChildrenFromCurrentMarriage() {
		return childrenFromCurrentMarriage;
	}

	public void setChildrenFromCurrentMarriage(Integer childrenFromCurrentMarriage) {
		this.childrenFromCurrentMarriage = childrenFromCurrentMarriage;
	}

	public Integer getChildrenFromPreviousMarriage() {
		return childrenFromPreviousMarriage;
	}

	public void setChildrenFromPreviousMarriage(Integer childrenFromPreviousMarriage) {
		this.childrenFromPreviousMarriage = childrenFromPreviousMarriage;
	}

	public Boolean getIsFirstMarriage() {
		return isFirstMarriage;
	}

	public void setIsFirstMarriage(Boolean isFirstMarriage) {
		this.isFirstMarriage = isFirstMarriage;
	}

	public Integer getCtsTestType() {
		return ctsTestType;
	}

	public void setCtsTestType(Integer ctsTestType) {
		this.ctsTestType = ctsTestType;
	}

	public String getCtsTestTypeText() {
		return ctsTestTypeText;
	}

	public void setCtsTestTypeText(String ctsTestTypeText) {
		this.ctsTestTypeText = ctsTestTypeText;
	}

	public LocalDate getCtsTestDate() {
		return ctsTestDate;
	}

	public void setCtsTestDate(LocalDate ctsTestDate) {
		this.ctsTestDate = ctsTestDate;
	}

	public Integer getCtsTestResultCode() {
		return ctsTestResultCode;
	}

	public void setCtsTestResultCode(Integer ctsTestResultCode) {
		this.ctsTestResultCode = ctsTestResultCode;
	}

	public String getCtsTestResultText() {
		return ctsTestResultText;
	}

	public void setCtsTestResultText(String ctsTestResultText) {
		this.ctsTestResultText = ctsTestResultText;
	}

	public LocalDate getCtsTest2Date() {
		return ctsTest2Date;
	}

	public void setCtsTest2Date(LocalDate ctsTest2Date) {
		this.ctsTest2Date = ctsTest2Date;
	}

	public String getDiagnosis1() {
		return diagnosis1;
	}

	public void setDiagnosis1(String diagnosis1) {
		this.diagnosis1 = diagnosis1;
	}

	public LocalDate getDiagnosisDate1() {
		return diagnosisDate1;
	}

	public void setDiagnosisDate1(LocalDate diagnosisDate1) {
		this.diagnosisDate1 = diagnosisDate1;
	}

	public String getDiagnosis2() {
		return diagnosis2;
	}

	public void setDiagnosis2(String diagnosis2) {
		this.diagnosis2 = diagnosis2;
	}

	public LocalDate getDiagnosisDate2() {
		return diagnosisDate2;
	}

	public void setDiagnosisDate2(LocalDate diagnosisDate2) {
		this.diagnosisDate2 = diagnosisDate2;
	}

	public LocalDate getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(LocalDate checkDate) {
		this.checkDate = checkDate;
	}

	public Integer getRightTesticlePosition() {
		return rightTesticlePosition;
	}

	public void setRightTesticlePosition(Integer rightTesticlePosition) {
		this.rightTesticlePosition = rightTesticlePosition;
	}

	public String getRightTesticlePositionText() {
		return rightTesticlePositionText;
	}

	public void setRightTesticlePositionText(String rightTesticlePositionText) {
		this.rightTesticlePositionText = rightTesticlePositionText;
	}

	public Integer getRightTesticleSize() {
		return rightTesticleSize;
	}

	public void setRightTesticleSize(Integer rightTesticleSize) {
		this.rightTesticleSize = rightTesticleSize;
	}

	public Integer getRightConistenitzia() {
		return rightConistenitzia;
	}

	public void setRightConistenitzia(Integer rightConistenitzia) {
		this.rightConistenitzia = rightConistenitzia;
	}

	public String getRightConistenitziaText() {
		return rightConistenitziaText;
	}

	public void setRightConistenitziaText(String rightConistenitziaText) {
		this.rightConistenitziaText = rightConistenitziaText;
	}

	public Integer getRightVaricocele() {
		return rightVaricocele;
	}

	public void setRightVaricocele(Integer rightVaricocele) {
		this.rightVaricocele = rightVaricocele;
	}

	public String getRightVaricoceleText() {
		return rightVaricoceleText;
	}

	public void setRightVaricoceleText(String rightVaricoceleText) {
		this.rightVaricoceleText = rightVaricoceleText;
	}

	public Integer getRightHydrocele() {
		return rightHydrocele;
	}

	public void setRightHydrocele(Integer rightHydrocele) {
		this.rightHydrocele = rightHydrocele;
	}

	public String getRightHydroceleText() {
		return rightHydroceleText;
	}

	public void setRightHydroceleText(String rightHydroceleText) {
		this.rightHydroceleText = rightHydroceleText;
	}

	public Integer getRightHernia() {
		return rightHernia;
	}

	public void setRightHernia(Integer rightHernia) {
		this.rightHernia = rightHernia;
	}

	public String getRightHerniaText() {
		return rightHerniaText;
	}

	public void setRightHerniaText(String rightHerniaText) {
		this.rightHerniaText = rightHerniaText;
	}

	public Integer getLeftTesticlePosition() {
		return leftTesticlePosition;
	}

	public void setLeftTesticlePosition(Integer leftTesticlePosition) {
		this.leftTesticlePosition = leftTesticlePosition;
	}

	public String getLeftTesticlePositionText() {
		return leftTesticlePositionText;
	}

	public void setLeftTesticlePositionText(String leftTesticlePositionText) {
		this.leftTesticlePositionText = leftTesticlePositionText;
	}

	public Integer getLeftTesticleSize() {
		return leftTesticleSize;
	}

	public void setLeftTesticleSize(Integer leftTesticleSize) {
		this.leftTesticleSize = leftTesticleSize;
	}

	public Integer getLeftConistenitzia() {
		return leftConistenitzia;
	}

	public void setLeftConistenitzia(Integer leftConistenitzia) {
		this.leftConistenitzia = leftConistenitzia;
	}

	public String getLeftConistenitziaText() {
		return leftConistenitziaText;
	}

	public void setLeftConistenitziaText(String leftConistenitziaText) {
		this.leftConistenitziaText = leftConistenitziaText;
	}

	public Integer getLeftVaricocele() {
		return leftVaricocele;
	}

	public void setLeftVaricocele(Integer leftVaricocele) {
		this.leftVaricocele = leftVaricocele;
	}

	public String getLeftVaricoceleText() {
		return leftVaricoceleText;
	}

	public void setLeftVaricoceleText(String leftVaricoceleText) {
		this.leftVaricoceleText = leftVaricoceleText;
	}

	public Integer getLeftHydrocele() {
		return leftHydrocele;
	}

	public void setLeftHydrocele(Integer leftHydrocele) {
		this.leftHydrocele = leftHydrocele;
	}

	public String getLeftHydroceleText() {
		return leftHydroceleText;
	}

	public void setLeftHydroceleText(String leftHydroceleText) {
		this.leftHydroceleText = leftHydroceleText;
	}

	public Integer getLeftHernia() {
		return leftHernia;
	}

	public void setLeftHernia(Integer leftHernia) {
		this.leftHernia = leftHernia;
	}

	public String getLeftHerniaText() {
		return leftHerniaText;
	}

	public void setLeftHerniaText(String leftHerniaText) {
		this.leftHerniaText = leftHerniaText;
	}

}
