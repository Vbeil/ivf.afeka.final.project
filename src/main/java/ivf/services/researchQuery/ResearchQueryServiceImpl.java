package ivf.services.researchQuery;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ivf.services.queryCache.QueryCacheKey;
import ivf.services.queryCache.QueryCacheService;
import ivf.util.ConsoleLogger;
import ivf.util.ProjectConfiguration;

@Service
public class ResearchQueryServiceImpl implements ResearchQueryService {

	private QueryCacheService queryCacheService;

	// Utilities
	private ObjectMapper jackson;
	private ConsoleLogger logger;

	@Autowired
	public ResearchQueryServiceImpl(QueryCacheService queryCacheService) {
		this.queryCacheService = queryCacheService;
	}

	@PostConstruct
	public void init() {
		this.jackson = new ObjectMapper();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm a z");
		this.jackson.setDateFormat(df);

		logger = new ConsoleLogger("Research Query");
	}

	@Override
	public String customQuery(String queryString) throws SQLException, JsonProcessingException, ExecutionException {
		queryString += "\nLIMIT " + ProjectConfiguration.MAX_RESULT_SET_SIZE;
		logger.Print("About to perform a custom research query ->\n" + queryString, true);

		List<Map<String, Object>> queryResult;

		QueryCacheKey queryKey = new QueryCacheKey(queryString);
		queryResult = queryCacheService.getFromQueryCache(queryKey);

		logger.PrintSeparation();
		logger.Print("Done querying the database", false);
		logger.Print("Found " + queryResult.size() + " results for this query", false);
		if (queryResult.isEmpty())
			return new String(jackson.writeValueAsString(""));

		return new String(jackson.writeValueAsString(queryResult));

	}

}
