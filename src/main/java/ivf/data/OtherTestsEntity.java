package ivf.data;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ivf.dataDeserializers.ExcelDateTimeDeserializer;
import ivf.dataDeserializers.IntegerDeserializer;
import ivf.dataDeserializers.StringNullCleanerDeserializer;

//Table #3 - Other Tests
@Entity
@Table(name = "OtherTests")
public class OtherTestsEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long rowId;

	@JsonProperty("idHash")
	private String patientId;

	@JsonDeserialize(using = ExcelDateTimeDeserializer.class)
	private LocalDateTime entryDate;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	@JsonProperty("admissionNoHash")
	private String admission;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer test;

	private String name;

	@JsonDeserialize(using = ExcelDateTimeDeserializer.class)
	@JsonAlias("techDate") // Needed for broken data provided by the hospital, Used only for deserializing. 
	private LocalDateTime testDate;
	
	public OtherTestsEntity() {

	}

	public OtherTestsEntity(String patientId, LocalDateTime entryDate, String admission, Integer test, String name,
			LocalDateTime testDate) {
		super();
		this.patientId = patientId;
		this.entryDate = entryDate;
		this.admission = admission;
		this.test = test;
		this.name = name;
		this.testDate = testDate;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public LocalDateTime getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(LocalDateTime entryDate) {
		this.entryDate = entryDate;
	}

	public String getAdmission() {
		return admission;
	}

	public void setAdmission(String admission) {
		this.admission = admission;
	}

	public Integer getTest() {
		return test;
	}

	public void setTest(Integer test) {
		this.test = test;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDateTime getTestDate() {
		return testDate;
	}

	public void setTestDate(LocalDateTime testDate) {
		this.testDate = testDate;
	}
	

}
