package ivf.services.queryCache;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Service;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.cache.Weigher;

import ivf.util.ConsoleLogger;
import ivf.util.ProjectConfiguration;

@Service
public class QueryCacheServiceImpl implements QueryCacheService {

	/**
	 * Gets the database connection URL from the configuration
	 */
	@Value("${spring.datasource.url}")
	private String dbConnectionURL;

	/**
	 * Gets the database connection User name from the configuration
	 */
	@Value("${spring.datasource.username}")
	private String dbUserName;

	/**
	 * Gets the database connection Password from the configuration
	 */
	@Value("${spring.datasource.password}")
	private String dbUserPass;

	private LoadingCache<QueryCacheKey, List<Map<String, Object>>> researchQueryCache;

	// Utils
	private ConsoleLogger logger;

	@PostConstruct
	public void init() {
		logger = new ConsoleLogger("Research Query Cache");

		Weigher<QueryCacheKey, List<Map<String, Object>>> weightBySize;
		weightBySize = new Weigher<QueryCacheKey, List<Map<String, Object>>>() {
			@Override
			public int weigh(QueryCacheKey key, List<Map<String, Object>> value) {
				return value.size();
			}
		};

		this.researchQueryCache = CacheBuilder.newBuilder()
				// .maximumSize(ProjectConfiguration.CACHE_MAX_SIZE)
				.expireAfterWrite(ProjectConfiguration.CACHE_MAX_LIFETIME_HOURS, TimeUnit.HOURS)
				.recordStats()
				.maximumWeight(ProjectConfiguration.CACHE_MAX_WEIGHT).weigher(weightBySize)
				.build(new CacheLoader<QueryCacheKey, List<Map<String, Object>>>() {
					@Override
					public List<Map<String, Object>> load(QueryCacheKey key) throws Exception {
						logger.PrintSeparation();
						logger.Print("Caching data result for query:\n" + key.getSqlQuery(), true);
						DriverManagerDataSource dataSource = new DriverManagerDataSource();
						dataSource.setUrl(dbConnectionURL);
						dataSource.setUsername(dbUserName);
						dataSource.setPassword(dbUserPass);
						JdbcTemplate jdbc = new JdbcTemplate(dataSource);
						return jdbc.queryForList(key.getSqlQuery());
					}
				});
	}

	public List<Map<String, Object>> getFromQueryCache(QueryCacheKey key) throws ExecutionException {
		List<Map<String, Object>> resultFromCache = researchQueryCache.getIfPresent(key);
		if (researchQueryCache.getIfPresent(key) != null) {
			logger.Print(
					"Saving precious computation time by returning " + resultFromCache.size() + " results from cache!",
					false);
			return resultFromCache;
		}
		return researchQueryCache.get(key); // Start a new transaction
	}

	public void clearQueryCache() {
		logger.Print("Clearing query cache...", true);
		researchQueryCache.invalidateAll();
	}
}
