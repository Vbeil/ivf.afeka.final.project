package ivf.dao;

import org.springframework.data.repository.CrudRepository;

import ivf.data.OPUEntity;

public interface OPUDao extends CrudRepository<OPUEntity, Long> {

}
