package ivf.dao;

import org.springframework.data.repository.CrudRepository;

import ivf.data.SpermEntity;

public interface SpermDao extends CrudRepository<SpermEntity, Long> {

}
