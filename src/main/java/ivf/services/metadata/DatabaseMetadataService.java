package ivf.services.metadata;
import java.sql.SQLException;

import com.fasterxml.jackson.core.JsonProcessingException;

import ivf.boundaries.TableColumnBoundary;

/**
 * The interface is responsible for handling the database's metadata
 */
public interface DatabaseMetadataService {

	/**
	 * Gets the table names of the user defined tables in the database
	 * 
	 * @return Returns a String type array of the table names
	 * @throws SQLException
	 */
	public String[] getDatabaseTables() throws SQLException;

	/**
	 * Gets the column names and the data type of the provided table
	 * 
	 * @param tableName The name of the required table in the database
	 * @return Returns a String type array of the required table's columns and their
	 *         data type
	 * @throws SQLException
	 * @throws JsonProcessingException
	 */
	public TableColumnBoundary[] getTableColumnsAndDataType(String tableName)
			throws SQLException, JsonProcessingException;
}
