package ivf.dao;

import org.springframework.data.repository.CrudRepository;

import ivf.data.ProtocolsDrugsEntity;

public interface ProtocolsDrugsDao extends CrudRepository<ProtocolsDrugsEntity, Long> {

}
