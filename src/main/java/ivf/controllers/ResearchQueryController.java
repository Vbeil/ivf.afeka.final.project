package ivf.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ivf.services.researchQuery.ResearchQueryService;
import ivf.util.ProjectConfiguration;

@RestController
public class ResearchQueryController {

	private ResearchQueryService researchQueryService;

	@Autowired
	public void setResearchQueryService(ResearchQueryService researchQueryService) {
		this.researchQueryService = researchQueryService;
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method = RequestMethod.POST, path = ProjectConfiguration.URL_RESEARCH_QUERY, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String performCustomQuery(@RequestBody String query) throws Exception {
		return researchQueryService.customQuery(query);
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method = RequestMethod.POST, path = ProjectConfiguration.URL_RESEARCH_QUERY_JSON, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String performCustomQuery(@RequestBody Map<String, Object> query) throws Exception {
		return researchQueryService.customQuery((String) query.get("query"));
	}
}
