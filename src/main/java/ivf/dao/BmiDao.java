package ivf.dao;

import org.springframework.data.repository.CrudRepository;

import ivf.data.BmiEntity;

public interface BmiDao extends CrudRepository<BmiEntity, Long> {

}
