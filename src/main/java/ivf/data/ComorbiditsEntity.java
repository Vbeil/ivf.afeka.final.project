package ivf.data;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ivf.dataDeserializers.ExcelDateDeserializer;
import ivf.dataDeserializers.StringNullCleanerDeserializer;

@Entity
@Table(name = "Comorbidits")
public class ComorbiditsEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long rowId;

	@JsonProperty("idHash")
	private String patientId;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	private LocalDate diagnosisDate;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String icd;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String name;

	public ComorbiditsEntity() {

	}

	public ComorbiditsEntity(String patientId, LocalDate diagnosisDate, String icd, String name) {
		super();
		this.patientId = patientId;
		this.diagnosisDate = diagnosisDate;
		this.icd = icd;
		this.name = name;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public LocalDate getDiagnosisDate() {
		return diagnosisDate;
	}

	public void setDiagnosisDate(LocalDate diagnosisDate) {
		this.diagnosisDate = diagnosisDate;
	}

	public String getIcd() {
		return icd;
	}

	public void setIcd(String icd) {
		this.icd = icd;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
