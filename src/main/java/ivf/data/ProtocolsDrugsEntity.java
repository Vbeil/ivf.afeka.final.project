package ivf.data;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ivf.dataDeserializers.ExcelDateDeserializer;
import ivf.dataDeserializers.ExcelDateTimeDeserializer;
import ivf.dataDeserializers.FloatDeserializer;
import ivf.dataDeserializers.IntegerDeserializer;
import ivf.dataDeserializers.StringNullCleanerDeserializer;

@Entity
@Table(name = "Protocols_Drugs")
public class ProtocolsDrugsEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long rowId;

	@JsonProperty("idHash")
	private String patientId;

	@JsonDeserialize(using = ExcelDateTimeDeserializer.class)
	private LocalDateTime entryDate;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	@JsonProperty("admissionNoHash")
	private String admission;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer protocol;

	@JsonDeserialize(using = ExcelDateTimeDeserializer.class)
	private LocalDateTime protocolEntryDate;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String fertilizationType;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer carePlanId;

	@JsonDeserialize(using = IntegerDeserializer.class)
	@JsonProperty("dayTechNum")
	private Integer dayTechNum1;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	private LocalDate date;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String name;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String wayOfGiving;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String dosageUnit;

	@JsonDeserialize(using = FloatDeserializer.class)
	private Float drugDosage;

	@JsonDeserialize(using = IntegerDeserializer.class)
	@JsonProperty("dayTechNum1")
	private Integer dayTechNum2; // ?

	public ProtocolsDrugsEntity() {

	}

	public ProtocolsDrugsEntity(String patientId, LocalDateTime entryDate, String admission, Integer protocol,
			LocalDateTime protocolEntryDate, String fetrilizationType, Integer carePlanId, Integer dayTechNum1,
			LocalDate date, String name, String wayOfGiving, String dosageUnit, Float drugDosage, Integer dayTechNum2) {
		super();
		this.patientId = patientId;
		this.entryDate = entryDate;
		this.admission = admission;
		this.protocol = protocol;
		this.protocolEntryDate = protocolEntryDate;
		this.fertilizationType = fetrilizationType;
		this.carePlanId = carePlanId;
		this.dayTechNum1 = dayTechNum1;
		this.date = date;
		this.name = name;
		this.wayOfGiving = wayOfGiving;
		this.dosageUnit = dosageUnit;
		this.drugDosage = drugDosage;
		this.dayTechNum2 = dayTechNum2;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public LocalDateTime getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(LocalDateTime entryDate) {
		this.entryDate = entryDate;
	}

	public String getAdmission() {
		return admission;
	}

	public void setAdmission(String admission) {
		this.admission = admission;
	}

	public Integer getProtocol() {
		return protocol;
	}

	public void setProtocol(Integer protocol) {
		this.protocol = protocol;
	}

	public LocalDateTime getProtocolEntryDate() {
		return protocolEntryDate;
	}

	public void setProtocolEntryDate(LocalDateTime protocolEntryDate) {
		this.protocolEntryDate = protocolEntryDate;
	}

	public String getFetrilizationType() {
		return fertilizationType;
	}

	public void setFetrilizationType(String fetrilizationType) {
		this.fertilizationType = fetrilizationType;
	}

	public Integer getCarePlanId() {
		return carePlanId;
	}

	public void setCarePlanId(Integer carePlanId) {
		this.carePlanId = carePlanId;
	}

	public Integer getDayTechNum1() {
		return dayTechNum1;
	}

	public void setDayTechNum1(Integer dayTechNum1) {
		this.dayTechNum1 = dayTechNum1;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getWayOfGiving() {
		return wayOfGiving;
	}

	public void setWayOfGiving(String wayOfGiving) {
		this.wayOfGiving = wayOfGiving;
	}

	public String getDosageUnit() {
		return dosageUnit;
	}

	public void setDosageUnit(String dosageUnit) {
		this.dosageUnit = dosageUnit;
	}

	public Float getDrugDosage() {
		return drugDosage;
	}

	public void setDrugDosage(Float drugDosage) {
		this.drugDosage = drugDosage;
	}

	public Integer getDayTechNum2() {
		return dayTechNum2;
	}

	public void setDayTechNum2(Integer dayTechNum2) {
		this.dayTechNum2 = dayTechNum2;
	}

}
