package ivf.data;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ivf.dataDeserializers.ExcelDateDeserializer;
import ivf.dataDeserializers.ExcelDateTimeDeserializer;
import ivf.dataDeserializers.StringNullCleanerDeserializer;

@Entity
@Table(name = "Protocols_Followup")
public class ProtocolsFollowupEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long rowId;

	@JsonProperty("idHash")
	private String patientId;

	@JsonDeserialize(using = ExcelDateTimeDeserializer.class)
	private LocalDateTime entryDate;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	@JsonProperty("admissionNoHash")
	private String admission;

	@JsonProperty("protocol")
	private Integer protocolCode;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	@JsonProperty("protocolText")
	private String protocolName;

	@JsonDeserialize(using = ExcelDateTimeDeserializer.class)
	private LocalDateTime protocolEntryDate;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String fertilizationType;

	private Integer carePlanId;

	private Integer dayTechNum;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	private LocalDate date;

	private Integer status;

	private String answerText;

	public ProtocolsFollowupEntity() {

	}

	public ProtocolsFollowupEntity(String patientId, LocalDateTime entryDate, String admission, Integer protocolCode,
			String protocolName, LocalDateTime protocolEntryDate, String fertilization, Integer carePlanId,
			Integer dayTechNum, LocalDate date, Integer status, String answerText) {
		super();
		this.patientId = patientId;
		this.entryDate = entryDate;
		this.admission = admission;
		this.protocolCode = protocolCode;
		this.protocolName = protocolName;
		this.protocolEntryDate = protocolEntryDate;
		this.fertilizationType = fertilization;
		this.carePlanId = carePlanId;
		this.dayTechNum = dayTechNum;
		this.date = date;
		this.status = status;
		this.answerText = answerText;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public LocalDateTime getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(LocalDateTime entryDate) {
		this.entryDate = entryDate;
	}

	public String getAdmission() {
		return admission;
	}

	public void setAdmission(String admission) {
		this.admission = admission;
	}

	public Integer getProtocolCode() {
		return protocolCode;
	}

	public void setProtocolCode(Integer protocolCode) {
		this.protocolCode = protocolCode;
	}

	public String getProtocolName() {
		return protocolName;
	}

	public void setProtocolName(String protocolName) {
		this.protocolName = protocolName;
	}

	public LocalDateTime getProtocolEntryDate() {
		return protocolEntryDate;
	}

	public void setProtocolEntryDate(LocalDateTime protocolEntryDate) {
		this.protocolEntryDate = protocolEntryDate;
	}

	public String getFertilization() {
		return fertilizationType;
	}

	public void setFertilization(String fertilization) {
		this.fertilizationType = fertilization;
	}

	public Integer getCarePlanId() {
		return carePlanId;
	}

	public void setCarePlanId(Integer carePlanId) {
		this.carePlanId = carePlanId;
	}

	public Integer getDayTechNum() {
		return dayTechNum;
	}

	public void setDayTechNum(Integer dayTechNum) {
		this.dayTechNum = dayTechNum;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getAnswerText() {
		return answerText;
	}

	public void setAnswerText(String answerText) {
		this.answerText = answerText;
	}

}
