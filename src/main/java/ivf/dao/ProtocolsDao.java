package ivf.dao;

import org.springframework.data.repository.CrudRepository;

import ivf.data.ProtocolsEntity;

public interface ProtocolsDao extends CrudRepository<ProtocolsEntity, Long> {

}
