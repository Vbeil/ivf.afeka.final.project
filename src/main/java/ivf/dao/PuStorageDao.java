package ivf.dao;

import org.springframework.data.repository.CrudRepository;

import ivf.data.PuStorageEntity;

public interface PuStorageDao extends CrudRepository<PuStorageEntity, Long> {

}
