package ivf.dao;

import org.springframework.data.repository.CrudRepository;

import ivf.data.ProtocolsFollowupEntity;

public interface ProtocolsFollowupDao extends CrudRepository<ProtocolsFollowupEntity, Long> {

}
