package ivf.services.tables;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ivf.dao.PatientDao;
import ivf.data.PatientEntity;
import ivf.util.NotFoundException;

@Service
public class PatientsServiceImpl implements PatientsService {
	private PatientDao patientDao;

	@Autowired
	public PatientsServiceImpl(PatientDao patientDao) {
		super();
		this.patientDao = patientDao;
	}

	@Override
	@Transactional
	public PatientEntity create(PatientEntity newPatient) throws Exception {
		try {
			PatientEntity entity = newPatient;
			entity = this.patientDao.save(entity); // save to db

			return entity;
		} catch (Exception e) {
			throw new RuntimeException();
		}

	}

	@Override
	@Transactional
	public PatientEntity[] create(PatientEntity[] newPatients) throws Exception {
		try {
			patientDao.saveAll(Arrays.asList(newPatients));
			return newPatients;

		} catch (Exception e) {
			throw new RuntimeException();
		}

	}

	@Override
	@Transactional
	public PatientEntity update(Long patientId, PatientEntity update) throws Exception {

		Optional<PatientEntity> exiting = this.patientDao.findById(patientId);

		if (exiting.isPresent()) {
			PatientEntity existingEntity = exiting.get();
			PatientEntity updateEntity = update;
			updateEntity.setPatientId(existingEntity.getPatientId());

			return this.patientDao.save(updateEntity);
		} else {
			throw new NotFoundException("patient with id: " + patientId + " could not be found");
		}

	}

	@Override
	@Transactional(readOnly = true)
	public List<PatientEntity> getAll() {
		return StreamSupport.stream(this.patientDao.findAll().spliterator(), false)
				.map(entity -> entity).collect(Collectors.toList());
	}

	@Override
	@Transactional(readOnly = true)
	public PatientEntity getSpecificPatient(Long patientId) throws Exception {

		Optional<PatientEntity> exiting = this.patientDao.findById(patientId);

		if (exiting.isPresent()) {
			return exiting.get();
		} else {
			throw new NotFoundException("patient with id: " + patientId + " could not be found");
		}

	}

	@Override
	@Transactional
	public void deleteAll() {
		this.patientDao.deleteAll();
	}

}
