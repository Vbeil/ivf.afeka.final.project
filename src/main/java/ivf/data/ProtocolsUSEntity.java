package ivf.data;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ivf.dataDeserializers.ExcelDateTimeDeserializer;
import ivf.dataDeserializers.FloatDeserializer;
import ivf.dataDeserializers.IntegerDeserializer;
import ivf.dataDeserializers.StringNullCleanerDeserializer;

@Entity
@Table(name = "Protocols_US")
public class ProtocolsUSEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long rowId;

	@JsonProperty("idHash")
	private String patientId;

	@JsonDeserialize(using = ExcelDateTimeDeserializer.class)
	private LocalDateTime entryDate;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	@JsonProperty("admissionNoHash")
	private String admission;

	@JsonProperty("admissionNo")
	private Long admissionNumber;

	private Integer protocol;

	private Integer carePlanId;

	@JsonDeserialize(using = ExcelDateTimeDeserializer.class)
	@JsonProperty("usDate")
	private LocalDateTime ultraSoundDate;

	private Integer usId;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String mucosaDescription;

	@JsonDeserialize(using = FloatDeserializer.class)
	private Float mucosaThickness;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer douglasLiquid;

	@JsonDeserialize(using = IntegerDeserializer.class)
	@JsonProperty("rowId")
	private Integer rowIdentifier;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer rightOvary;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer leftOvary;

	public ProtocolsUSEntity() {

	}

	public ProtocolsUSEntity(String patientId, LocalDateTime entryDate, String admission, Long admissionNumber,
			Integer protocol, Integer carePlanId, LocalDateTime ultraSoundDate, Integer usId, String mucosaDescription,
			Float mucosaThickness, Integer douglasLiquid, Integer rowNumber, Integer rightOvary, Integer leftOvary) {
		super();
		this.patientId = patientId;
		this.entryDate = entryDate;
		this.admission = admission;
		this.admissionNumber = admissionNumber;
		this.protocol = protocol;
		this.carePlanId = carePlanId;
		this.ultraSoundDate = ultraSoundDate;
		this.usId = usId;
		this.mucosaDescription = mucosaDescription;
		this.mucosaThickness = mucosaThickness;
		this.douglasLiquid = douglasLiquid;
		this.rowIdentifier = rowNumber;
		this.rightOvary = rightOvary;
		this.leftOvary = leftOvary;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public LocalDateTime getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(LocalDateTime entryDate) {
		this.entryDate = entryDate;
	}

	public String getAdmission() {
		return admission;
	}

	public void setAdmission(String admission) {
		this.admission = admission;
	}

	public Long getAdmissionNumber() {
		return admissionNumber;
	}

	public void setAdmissionNumber(Long admissionNumber) {
		this.admissionNumber = admissionNumber;
	}

	public Integer getProtocol() {
		return protocol;
	}

	public void setProtocol(Integer protocol) {
		this.protocol = protocol;
	}

	public Integer getCarePlanId() {
		return carePlanId;
	}

	public void setCarePlanId(Integer carePlanId) {
		this.carePlanId = carePlanId;
	}

	public LocalDateTime getUltraSoundDate() {
		return ultraSoundDate;
	}

	public void setUltraSoundDate(LocalDateTime ultraSoundDate) {
		this.ultraSoundDate = ultraSoundDate;
	}

	public Integer getUsId() {
		return usId;
	}

	public void setUsId(Integer usId) {
		this.usId = usId;
	}

	public String getMucosaDescription() {
		return mucosaDescription;
	}

	public void setMucosaDescription(String mucosaDescription) {
		this.mucosaDescription = mucosaDescription;
	}

	public Float getMucosaThickness() {
		return mucosaThickness;
	}

	public void setMucosaThickness(Float mucosaThickness) {
		this.mucosaThickness = mucosaThickness;
	}

	public Integer getDouglasLiquid() {
		return douglasLiquid;
	}

	public void setDouglasLiquid(Integer douglasLiquid) {
		this.douglasLiquid = douglasLiquid;
	}

	public Integer getRowNumber() {
		return rowIdentifier;
	}

	public void setRowNumber(Integer rowNumber) {
		this.rowIdentifier = rowNumber;
	}

	public Integer getRightOvary() {
		return rightOvary;
	}

	public void setRightOvary(Integer rightOvary) {
		this.rightOvary = rightOvary;
	}

	public Integer getLeftOvary() {
		return leftOvary;
	}

	public void setLeftOvary(Integer leftOvary) {
		this.leftOvary = leftOvary;
	}

}
