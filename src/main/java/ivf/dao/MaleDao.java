package ivf.dao;

import org.springframework.data.repository.CrudRepository;

import ivf.data.MaleEntity;

public interface MaleDao extends CrudRepository<MaleEntity, Long> {

}
