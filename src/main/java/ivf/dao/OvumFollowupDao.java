package ivf.dao;

import org.springframework.data.repository.CrudRepository;

import ivf.data.OvumFollowupEntity;

public interface OvumFollowupDao extends CrudRepository<OvumFollowupEntity, Long> {

}
