package ivf.data;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ivf.dataDeserializers.ExcelDateTimeDeserializer;
import ivf.dataDeserializers.IntegerDeserializer;
import ivf.dataDeserializers.StringNullCleanerDeserializer;

@Entity
@Table(name = "ReturnAction")
public class ReturnActionEntity {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long rowId;

	@JsonProperty("idHash")
	private String patientId;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	@JsonProperty("admissionNoHash")
	private String admission;

	@JsonDeserialize(using = ExcelDateTimeDeserializer.class)
	private LocalDateTime returnActionDate;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer trpProper;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String trpProperText;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer trpCatheter;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String trpCatheterText;

	public ReturnActionEntity() {
	}

	public ReturnActionEntity(String patientId, String admission, LocalDateTime returnActionDate, Integer trpProper,
			String trpProperText, Integer trpCatheter, String trpCatheterText) {
		super();
		this.patientId = patientId;
		this.admission = admission;
		this.returnActionDate = returnActionDate;
		this.trpProper = trpProper;
		this.trpProperText = trpProperText;
		this.trpCatheter = trpCatheter;
		this.trpCatheterText = trpCatheterText;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public String getAdmission() {
		return admission;
	}

	public void setAdmission(String admission) {
		this.admission = admission;
	}

	public LocalDateTime getReturnActionDate() {
		return returnActionDate;
	}

	public void setReturnActionDate(LocalDateTime returnActionDate) {
		this.returnActionDate = returnActionDate;
	}

	public Integer getTrpProper() {
		return trpProper;
	}

	public void setTrpProper(Integer trpProper) {
		this.trpProper = trpProper;
	}

	public String getTrpProperText() {
		return trpProperText;
	}

	public void setTrpProperText(String trpProperText) {
		this.trpProperText = trpProperText;
	}

	public Integer getTrpCatheter() {
		return trpCatheter;
	}

	public void setTrpCatheter(Integer trpCatheter) {
		this.trpCatheter = trpCatheter;
	}

	public String getTrpCatheterText() {
		return trpCatheterText;
	}

	public void setTrpCatheterText(String trpCatheterText) {
		this.trpCatheterText = trpCatheterText;
	}

}
