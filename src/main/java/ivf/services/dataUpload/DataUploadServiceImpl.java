package ivf.services.dataUpload;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ivf.dao.AccessDataDao;
import ivf.dao.BmiDao;
import ivf.dao.ComorbiditsDao;
import ivf.dao.DiagnosisDao;
import ivf.dao.FemaleMaleConnectionDao;
import ivf.dao.HabitsDao;
import ivf.dao.IVFHistoryDao;
import ivf.dao.MaleDao;
import ivf.dao.OPUDao;
import ivf.dao.OPUDrugsDao;
import ivf.dao.OtherTestsDao;
import ivf.dao.OvumFollowupDao;
import ivf.dao.PatientDao;
import ivf.dao.ProtocolsDao;
import ivf.dao.ProtocolsDrugsDao;
import ivf.dao.ProtocolsFollowupDao;
import ivf.dao.ProtocolsProceduresDao;
import ivf.dao.ProtocolsUSDao;
import ivf.dao.PuStorageDao;
import ivf.dao.ReturnActionDao;
import ivf.dao.ReturnActionDrugsDao;
import ivf.dao.SpermDao;
import ivf.dao.TreatmentSummaryDao;
import ivf.dao.UsPregnancyDao;
import ivf.data.AccessEntity;
import ivf.data.BmiEntity;
import ivf.data.ComorbiditsEntity;
import ivf.data.DiagnosisEntity;
import ivf.data.FemaleMaleConnectionEntity;
import ivf.data.HabitsEntity;
import ivf.data.IVFHistoryEntity;
import ivf.data.MaleEntity;
import ivf.data.OPUDrugsEntity;
import ivf.data.OPUEntity;
import ivf.data.OtherTestsEntity;
import ivf.data.OvumFollowupEntity;
import ivf.data.PatientEntity;
import ivf.data.ProtocolsDrugsEntity;
import ivf.data.ProtocolsEntity;
import ivf.data.ProtocolsFollowupEntity;
import ivf.data.ProtocolsProceduresEntity;
import ivf.data.ProtocolsUSEntity;
import ivf.data.PuStorageEntity;
import ivf.data.ReturnActionDrugsEntity;
import ivf.data.ReturnActionEntity;
import ivf.data.SpermEntity;
import ivf.data.TreatmentSummaryEntity;
import ivf.data.UsPregnancyEntity;
import ivf.services.queryCache.QueryCacheService;
import ivf.util.ConsoleLogger;

@Service
public class DataUploadServiceImpl implements DataUploadService {

	private QueryCacheService queryCacheService;
	private ConsoleLogger logger;

	private PatientDao patientsHandler;
	private IVFHistoryDao ivfHistoryHandler;
	private OtherTestsDao otherTestsHandler;
	private DiagnosisDao diagnosisHandler;
	private ProtocolsDao protocolsHandler;
	private ProtocolsFollowupDao protocolsFollowupHandler;
	private ProtocolsProceduresDao protocolsProceduresHandler;
	private ProtocolsUSDao protocolsUsHandler;
	private ProtocolsDrugsDao protocolsDrugsHandler;
	private OPUDao opuHandler;
	private OPUDrugsDao opuDrugsHandler;
	private FemaleMaleConnectionDao femaleMaleConnectionHandler;
	private ReturnActionDao returnActionHandler;
	private ReturnActionDrugsDao returnActionDrugsHandler;
	private SpermDao spermHandler;
	private OvumFollowupDao ovumFollowupHandler;
	private PuStorageDao puStorageHandler;
	private TreatmentSummaryDao treatmentSummaryHandler;
	private UsPregnancyDao usPregnancyHandler;
	private MaleDao maleHandler;
	private ComorbiditsDao comorbiditsHandler;
	private HabitsDao habitsHandler;
	private BmiDao bmiHandler;
	private AccessDataDao accessHandler;


	@Autowired
	public DataUploadServiceImpl(QueryCacheService queryCacheService, PatientDao patientsHandler,
			IVFHistoryDao ivfHistoryHandler, OtherTestsDao otherTestsHandler, DiagnosisDao diagnosisHandler,
			ProtocolsDao protocolsHandler, ProtocolsFollowupDao protocolsFollowupHandler,
			ProtocolsProceduresDao protocolsProceduresHandler, ProtocolsUSDao protocolsUsHandler,
			ProtocolsDrugsDao protocolsDrugsHandler, OPUDao opuHandler, OPUDrugsDao opuDrugsHandler,
			FemaleMaleConnectionDao femaleMaleConnectionHandler, ReturnActionDao returnActionHandler,
			ReturnActionDrugsDao returnActionDrugsHandler, SpermDao spermHandler, OvumFollowupDao ovumFollowupHandler,
			PuStorageDao puStorageHandler, TreatmentSummaryDao treatmentSummaryHandler,
			UsPregnancyDao usPregnancyHandler, MaleDao maleHandler, ComorbiditsDao comorbiditsHandler,
			HabitsDao habitsHandler, BmiDao bmiHandler, AccessDataDao accessHandler) {
		super();
		this.queryCacheService = queryCacheService;
		this.patientsHandler = patientsHandler;
		this.ivfHistoryHandler = ivfHistoryHandler;
		this.otherTestsHandler = otherTestsHandler;
		this.diagnosisHandler = diagnosisHandler;
		this.protocolsHandler = protocolsHandler;
		this.protocolsFollowupHandler = protocolsFollowupHandler;
		this.protocolsProceduresHandler = protocolsProceduresHandler;
		this.protocolsUsHandler = protocolsUsHandler;
		this.protocolsDrugsHandler = protocolsDrugsHandler;
		this.opuHandler = opuHandler;
		this.opuDrugsHandler = opuDrugsHandler;
		this.femaleMaleConnectionHandler = femaleMaleConnectionHandler;
		this.returnActionHandler = returnActionHandler;
		this.returnActionDrugsHandler = returnActionDrugsHandler;
		this.spermHandler = spermHandler;
		this.ovumFollowupHandler = ovumFollowupHandler;
		this.puStorageHandler = puStorageHandler;
		this.treatmentSummaryHandler = treatmentSummaryHandler;
		this.usPregnancyHandler = usPregnancyHandler;
		this.maleHandler = maleHandler;
		this.comorbiditsHandler = comorbiditsHandler;
		this.habitsHandler = habitsHandler;
		this.bmiHandler = bmiHandler;
		this.accessHandler = accessHandler;
		logger = new ConsoleLogger("Data Upload Service");
	}

	@Override
	@Transactional
	public void uploadVisitsTable(PatientEntity[] patients, String override) {
		insertDataIntoDB(patients, override, "Patients", patientsHandler);
	}

	@Override
	@Transactional
	public void uploadIVFHistoryTable(IVFHistoryEntity[] ivfHistoryEntries, String override) {
		insertDataIntoDB(ivfHistoryEntries, override, "IVF History", ivfHistoryHandler);
	}

	@Override
	@Transactional
	public void uploadOtherTestsTable(OtherTestsEntity[] otherTestsEntries, String override) {
		insertDataIntoDB(otherTestsEntries, override, "Other Tests", otherTestsHandler);
	}

	@Override
	@Transactional
	public void uploadDiagnosisTable(DiagnosisEntity[] diagnosisEntries, String override) {
		insertDataIntoDB(diagnosisEntries, override, "Diagnosis", diagnosisHandler);
	}

	@Override
	@Transactional
	public void uploadProtocolsTable(ProtocolsEntity[] protocolEntries, String override) {
		insertDataIntoDB(protocolEntries, override, "Protocols", protocolsHandler);
	}

	@Override
	@Transactional
	public void uploadProtocolsFollowupTable(ProtocolsFollowupEntity[] protocolFollowupEntries, String override) {
		insertDataIntoDB(protocolFollowupEntries, override, "Protocols Followup", protocolsFollowupHandler);
	}

	@Override
	@Transactional
	public void uploadProtocolsProceduresTable(ProtocolsProceduresEntity[] protocolProceduresEntries, String override) {
		insertDataIntoDB(protocolProceduresEntries, override, "Protocols Procedures", protocolsProceduresHandler);
	}

	@Override
	@Transactional
	public void uploadProtocolsUSTable(ProtocolsUSEntity[] protocolUsEntries, String override) {
		insertDataIntoDB(protocolUsEntries, override, "Protocols US", protocolsUsHandler);
	}

	@Override
	@Transactional
	public void uploadProtocolDrugsTable(ProtocolsDrugsEntity[] protocolDrugsEntries, String override) {
		insertDataIntoDB(protocolDrugsEntries, override, "Protocols Drugs", protocolsDrugsHandler);
	}

	@Override
	public void uploadOPUTable(OPUEntity[] opuEntries, String override) {
		insertDataIntoDB(opuEntries, override, "OPU", opuHandler);
	}

	@Override
	public void uploadOPUDrugsTable(OPUDrugsEntity[] opuDrugsEntries, String override) {
		insertDataIntoDB(opuDrugsEntries, override, "OPU Drugs", opuDrugsHandler);
	}

	@Override
	public void uploadReturnActionTable(ReturnActionEntity[] returnActionEntries, String override) {
		insertDataIntoDB(returnActionEntries, override, "Return Action", returnActionHandler);
	}

	@Override
	public void uploadReturnActionDrugsTable(ReturnActionDrugsEntity[] returnActionDrugsEntries, String override) {
		insertDataIntoDB(returnActionDrugsEntries, override, "Return Action Drugs", returnActionDrugsHandler);
	}

	@Override
	public void uploadSpermTable(SpermEntity[] spermEntries, String override) {
		insertDataIntoDB(spermEntries, override, "Semen", spermHandler);
	}

	@Override
	public void uploadOvumFollowupTable(OvumFollowupEntity[] ovumFollowupEntries, String override) {
		insertDataIntoDB(ovumFollowupEntries, override, "Ovum Followup", ovumFollowupHandler);
	}

	@Override
	@Transactional
	public void uploadPuStorageTable(PuStorageEntity[] puStorageEntries, String override) {
		insertDataIntoDB(puStorageEntries, override, "PuStorage", puStorageHandler);
	}

	@Override
	@Transactional
	public void uploadTreatmentSummaryTable(TreatmentSummaryEntity[] treatmentSummaryEntries, String override) {
		insertDataIntoDB(treatmentSummaryEntries, override, "TreatmentSummary", treatmentSummaryHandler);
	}


	@Override
	@Transactional
	public void uploadUsPregnancyTable(UsPregnancyEntity[] usPregnancyEntries, String override) {
		insertDataIntoDB(usPregnancyEntries, override, "UsPregnancy", usPregnancyHandler);
	}

	@Override
	@Transactional
	public void uploadMaleTable(MaleEntity[] maleEntries, String override) {
		insertDataIntoDB(maleEntries, override, "Male", maleHandler);
	}


	@Override
	@Transactional
	public void uploadComorbiditsTable(ComorbiditsEntity[] comorbiditsEntries, String override) {
		insertDataIntoDB(comorbiditsEntries, override, "Comorbidits", comorbiditsHandler);
	}

	@Override
	@Transactional
	public void uploadHabitsTable(HabitsEntity[] habitsEntries, String override) {
		insertDataIntoDB(habitsEntries, override, "Habits", habitsHandler);
	}

	@Override
	@Transactional
	public void uploadBmiTable(BmiEntity[] bmiEntries, String override) {
		insertDataIntoDB(bmiEntries, override, "BMI", bmiHandler);
	}

	@Override
	@Transactional
	public void uploadAccessTable(AccessEntity[] accessPatients, String override) {
		insertDataIntoDB(accessPatients, override, "Access Data", accessHandler);
	}

	@Override
	public void uploadFemaleMaleConnectionTable(FemaleMaleConnectionEntity[] femaleMaleConnectionEntries,
			String override) {
		insertDataIntoDB(femaleMaleConnectionEntries, override, "Female Male Connection", femaleMaleConnectionHandler);
	}

	@Override
	public void clearDatabaseAndCache() {
		logger.Print("Clearing Database and Cache", true);
		queryCacheService.clearQueryCache();
		patientsHandler.deleteAll();
		ivfHistoryHandler.deleteAll();
		otherTestsHandler.deleteAll();
		diagnosisHandler.deleteAll();
		protocolsHandler.deleteAll();
		protocolsFollowupHandler.deleteAll();
		protocolsProceduresHandler.deleteAll();
		protocolsUsHandler.deleteAll();
		protocolsDrugsHandler.deleteAll();
		opuHandler.deleteAll();
		returnActionHandler.deleteAll();
		spermHandler.deleteAll();
		ovumFollowupHandler.deleteAll();
		maleHandler.deleteAll();
		treatmentSummaryHandler.deleteAll();
		puStorageHandler.deleteAll();
		usPregnancyHandler.deleteAll();
		comorbiditsHandler.deleteAll();
		habitsHandler.deleteAll();
		bmiHandler.deleteAll();
		accessHandler.deleteAll();
		femaleMaleConnectionHandler.deleteAll();
		logger.Print("Done clearing Database and Cache", true);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void insertDataIntoDB(Object[] objects, String override, String tableName, CrudRepository databaseTable) {
		if (override != null && override.equalsIgnoreCase("override")) {
			logger.Print("Removing all data from table [ " + tableName + " ]", true);
			databaseTable.deleteAll();
			logger.Print("Finished removing all data from table [ " + tableName + " ]", true);
		}
		try {
			logger.Print("Saving " + objects.length + " new objects to table [ " + tableName + " ]", true);
			databaseTable.saveAll(Arrays.asList(objects));
			queryCacheService.clearQueryCache();
			logger.Print("Finished saving " + objects.length + " new objects to table [ " + tableName + " ]", true);

		} catch (Exception e) {
			throw new RuntimeException();
		}

	}

}
