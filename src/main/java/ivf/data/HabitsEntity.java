package ivf.data;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ivf.dataDeserializers.ExcelDateDeserializer;
import ivf.dataDeserializers.YesNoToBooleanDeserializer;

@Entity
@Table(name = "Habits")
public class HabitsEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long rowId;

	@JsonProperty("idHash")
	private String patientId;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	private LocalDate entryDate;

	@JsonProperty("smoking1Yes2No")
	@JsonDeserialize(using = YesNoToBooleanDeserializer.class)
	private Boolean smoking;

	@JsonProperty("alcohol1Yes2No")
	@JsonDeserialize(using = YesNoToBooleanDeserializer.class)
	private Boolean alcohol;

	@JsonProperty("drugs1Yes2No")
	@JsonDeserialize(using = YesNoToBooleanDeserializer.class)
	private Boolean drugs;

	public HabitsEntity() {

	}

	public HabitsEntity(String patientId, LocalDate entryDate, Boolean smoking, Boolean alcohol, Boolean drugs) {
		super();
		this.patientId = patientId;
		this.entryDate = entryDate;
		this.smoking = smoking;
		this.alcohol = alcohol;
		this.drugs = drugs;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public LocalDate getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(LocalDate entryDate) {
		this.entryDate = entryDate;
	}

	public Boolean getSmoking() {
		return smoking;
	}

	public void setSmoking(Boolean smoking) {
		this.smoking = smoking;
	}

	public Boolean getAlcohol() {
		return alcohol;
	}

	public void setAlcohol(Boolean alcohol) {
		this.alcohol = alcohol;
	}

	public Boolean getDrugs() {
		return drugs;
	}

	public void setDrugs(Boolean drugs) {
		this.drugs = drugs;
	}

}
