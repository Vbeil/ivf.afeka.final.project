package ivf.controllers;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

import ivf.boundaries.TableColumnBoundary;
import ivf.services.metadata.DatabaseMetadataService;
import ivf.util.ProjectConfiguration;

@RestController
public class TablesController {

	private DatabaseMetadataService databaseMetadataService;

	@Autowired
	public void setMetadataHandler(DatabaseMetadataService databaseMetadataService) {
		this.databaseMetadataService = databaseMetadataService;
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method = RequestMethod.GET, path = ProjectConfiguration.URL_TABLES, produces = MediaType.APPLICATION_JSON_VALUE)
	public String[] getDatabaseTables() throws SQLException {
		return databaseMetadataService.getDatabaseTables();
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method = RequestMethod.GET, path = ProjectConfiguration.URL_TABLE_COLUMNS, produces = MediaType.APPLICATION_JSON_VALUE)
	public TableColumnBoundary[] getTableColumnsAndDataType(@PathVariable("tableName") String tableName)
			throws SQLException, JsonProcessingException {
		return databaseMetadataService.getTableColumnsAndDataType(tableName.toUpperCase());
	}
}
