package ivf.dataDeserializers;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class IntegerDeserializer extends JsonDeserializer<Integer> {

	@Override
	public Integer deserialize(JsonParser parser, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {

		String intStr = parser.getText();

		if (intStr.isEmpty() || intStr == null || intStr.equalsIgnoreCase("NULL")) {
			return null;
		}

		return new Integer(intStr);
	}
}
