package ivf.dao;

import org.springframework.data.repository.CrudRepository;

import ivf.data.ComorbiditsEntity;

public interface ComorbiditsDao extends CrudRepository<ComorbiditsEntity, Long> {

}
