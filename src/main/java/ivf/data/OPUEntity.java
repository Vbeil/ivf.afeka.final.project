package ivf.data;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ivf.dataDeserializers.ExcelDateDeserializer;
import ivf.dataDeserializers.ExcelDateTimeDeserializer;
import ivf.dataDeserializers.IntegerDeserializer;
import ivf.dataDeserializers.StringNullCleanerDeserializer;

@Entity
@Table(name = "OPU")
public class OPUEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long rowId;

	@JsonProperty("idHash")
	private String patientId;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	@JsonProperty("admissionNoHash")
	private String admission;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	private LocalDate plannedActionDate;

	@JsonDeserialize(using = ExcelDateTimeDeserializer.class)
	private LocalDateTime opuDate;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer eupModerated;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String eupModeratedText;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer eupPumping;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String eupPumpingText;

	public OPUEntity() {
	}

	public OPUEntity(String patientId, String admission, LocalDate plannedActionDate, LocalDateTime opuDate,
			Integer eupModerated, String eupModeratedText, Integer eupPumping, String eupPumpingText) {
		super();
		this.patientId = patientId;
		this.admission = admission;
		this.plannedActionDate = plannedActionDate;
		this.opuDate = opuDate;
		this.eupModerated = eupModerated;
		this.eupModeratedText = eupModeratedText;
		this.eupPumping = eupPumping;
		this.eupPumpingText = eupPumpingText;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public String getAdmission() {
		return admission;
	}

	public void setAdmission(String admission) {
		this.admission = admission;
	}

	public LocalDate getPlannedActionDate() {
		return plannedActionDate;
	}

	public void setPlannedActionDate(LocalDate plannedActionDate) {
		this.plannedActionDate = plannedActionDate;
	}

	public LocalDateTime getOpuDate() {
		return opuDate;
	}

	public void setOpuDate(LocalDateTime opuDate) {
		this.opuDate = opuDate;
	}

	public Integer getEupModerated() {
		return eupModerated;
	}

	public void setEupModerated(Integer eupModerated) {
		this.eupModerated = eupModerated;
	}

	public String getEupModeratedText() {
		return eupModeratedText;
	}

	public void setEupModeratedText(String eupModeratedText) {
		this.eupModeratedText = eupModeratedText;
	}

	public Integer getEupPumping() {
		return eupPumping;
	}

	public void setEupPumping(Integer eupPumping) {
		this.eupPumping = eupPumping;
	}

	public String getEupPumpingText() {
		return eupPumpingText;
	}

	public void setEupPumpingText(String eupPumpingText) {
		this.eupPumpingText = eupPumpingText;
	}
}
