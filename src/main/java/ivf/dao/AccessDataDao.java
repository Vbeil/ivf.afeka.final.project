package ivf.dao;

import org.springframework.data.repository.CrudRepository;

import ivf.data.AccessEntity;

public interface AccessDataDao extends CrudRepository<AccessEntity, Long> {

}
