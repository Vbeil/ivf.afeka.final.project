package ivf.data;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ivf.dataDeserializers.ExcelDateDeserializer;
import ivf.dataDeserializers.FloatDeserializer;
import ivf.dataDeserializers.IntegerDeserializer;
import ivf.dataDeserializers.StringNullCleanerDeserializer;
import ivf.dataDeserializers.YesNoToBooleanDeserializer;

@Entity
@Table(name = "UsPregnancy")
public class UsPregnancyEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long rowId;

	@JsonProperty("idHash")
	private String patientId;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String b1;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	private LocalDate dayB1;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer b1Val;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String b2;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	@JsonProperty("dayb2")
	private LocalDate dayB2;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer b2Val;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String b3;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	private LocalDate dayB3;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String b3Val;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	private LocalDate dayReturn;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	private LocalDate dayReturn2; // Problem - All NULLS

	@JsonDeserialize(using = YesNoToBooleanDeserializer.class)
	@JsonProperty("pregnancy1Y2N")
	private Boolean pregnancy;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	private LocalDate pregnancyDate;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	@JsonProperty("testDate")
	private LocalDate testDate1;

	@JsonDeserialize(using = ExcelDateDeserializer.class)
	@JsonProperty("testDate1")
	private LocalDate testDate2;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer wombSac;

	@JsonDeserialize(using = IntegerDeserializer.class)
	private Integer fetusNum;

	@JsonDeserialize(using = YesNoToBooleanDeserializer.class)
	private Boolean pulse;

	@JsonDeserialize(using = FloatDeserializer.class)
	private Float amountOfWater; // Problem - All NULLS

	@JsonDeserialize(using = FloatDeserializer.class)
	private Float cervicalLength; // Problem - All NULLS

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String placentalLocation; // Problem - All NULLS

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String pregnancyAge;

	@JsonDeserialize(using = StringNullCleanerDeserializer.class)
	private String fetalMovements;

	@JsonDeserialize(using = YesNoToBooleanDeserializer.class)
	private Boolean fetalPole;

	@JsonDeserialize(using = YesNoToBooleanDeserializer.class)
	private Boolean yolkSac;

	@JsonDeserialize(using = FloatDeserializer.class)
	private Float forBiometricsWeek; // Problem - All NULLS

	@JsonDeserialize(using = FloatDeserializer.class)
	private Float forWeek;

	public UsPregnancyEntity() {

	}

	public UsPregnancyEntity(String patientId, String b1, LocalDate dayB1, Integer b1Val, String b2, LocalDate dayB2,
			Integer b2Val, String b3, LocalDate dayB3, String b3Val, LocalDate dayReturn, LocalDate dayReturn2,
			Boolean pregnancy, LocalDate pregnancyDate, LocalDate testDate1, LocalDate testDate2, Integer wombSac,
			Integer fetusNum, Boolean pulse, Float amountOfWater, Float cervicalLength, String placentalLocation,
			String pregnancyAge, String fetalMovements, Boolean fetalPole, Boolean yolkSac, Float forBiometricsWeek,
			Float forWeek) {
		super();
		this.patientId = patientId;
		this.b1 = b1;
		this.dayB1 = dayB1;
		this.b1Val = b1Val;
		this.b2 = b2;
		this.dayB2 = dayB2;
		this.b2Val = b2Val;
		this.b3 = b3;
		this.dayB3 = dayB3;
		this.b3Val = b3Val;
		this.dayReturn = dayReturn;
		this.dayReturn2 = dayReturn2;
		this.pregnancy = pregnancy;
		this.pregnancyDate = pregnancyDate;
		this.testDate1 = testDate1;
		this.testDate2 = testDate2;
		this.wombSac = wombSac;
		this.fetusNum = fetusNum;
		this.pulse = pulse;
		this.amountOfWater = amountOfWater;
		this.cervicalLength = cervicalLength;
		this.placentalLocation = placentalLocation;
		this.pregnancyAge = pregnancyAge;
		this.fetalMovements = fetalMovements;
		this.fetalPole = fetalPole;
		this.yolkSac = yolkSac;
		this.forBiometricsWeek = forBiometricsWeek;
		this.forWeek = forWeek;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public String getB1() {
		return b1;
	}

	public void setB1(String b1) {
		this.b1 = b1;
	}

	public LocalDate getDayB1() {
		return dayB1;
	}

	public void setDayB1(LocalDate dayB1) {
		this.dayB1 = dayB1;
	}

	public Integer getB1Val() {
		return b1Val;
	}

	public void setB1Val(Integer b1Val) {
		this.b1Val = b1Val;
	}

	public String getB2() {
		return b2;
	}

	public void setB2(String b2) {
		this.b2 = b2;
	}

	public LocalDate getDayB2() {
		return dayB2;
	}

	public void setDayB2(LocalDate dayB2) {
		this.dayB2 = dayB2;
	}

	public Integer getB2Val() {
		return b2Val;
	}

	public void setB2Val(Integer b2Val) {
		this.b2Val = b2Val;
	}

	public String getB3() {
		return b3;
	}

	public void setB3(String b3) {
		this.b3 = b3;
	}

	public LocalDate getDayB3() {
		return dayB3;
	}

	public void setDayB3(LocalDate dayB3) {
		this.dayB3 = dayB3;
	}

	public String getB3Val() {
		return b3Val;
	}

	public void setB3Val(String b3Val) {
		this.b3Val = b3Val;
	}

	public LocalDate getDayReturn() {
		return dayReturn;
	}

	public void setDayReturn(LocalDate dayReturn) {
		this.dayReturn = dayReturn;
	}

	public LocalDate getDayReturn2() {
		return dayReturn2;
	}

	public void setDayReturn2(LocalDate dayReturn2) {
		this.dayReturn2 = dayReturn2;
	}

	public Boolean getPregnancy() {
		return pregnancy;
	}

	public void setPregnancy(Boolean pregnancy) {
		this.pregnancy = pregnancy;
	}

	public LocalDate getPregnancyDate() {
		return pregnancyDate;
	}

	public void setPregnancyDate(LocalDate pregnancyDate) {
		this.pregnancyDate = pregnancyDate;
	}

	public LocalDate getTestDate1() {
		return testDate1;
	}

	public void setTestDate1(LocalDate testDate1) {
		this.testDate1 = testDate1;
	}

	public LocalDate getTestDate2() {
		return testDate2;
	}

	public void setTestDate2(LocalDate testDate2) {
		this.testDate2 = testDate2;
	}

	public Integer getWombSac() {
		return wombSac;
	}

	public void setWombSac(Integer wombSac) {
		this.wombSac = wombSac;
	}

	public Integer getFetusNum() {
		return fetusNum;
	}

	public void setFetusNum(Integer fetusNum) {
		this.fetusNum = fetusNum;
	}

	public Boolean getPulse() {
		return pulse;
	}

	public void setPulse(Boolean pulse) {
		this.pulse = pulse;
	}

	public Float getAmountOfWater() {
		return amountOfWater;
	}

	public void setAmountOfWater(Float amountOfWater) {
		this.amountOfWater = amountOfWater;
	}

	public Float getCervicalLength() {
		return cervicalLength;
	}

	public void setCervicalLength(Float cervicalLength) {
		this.cervicalLength = cervicalLength;
	}

	public String getPlacentalLocation() {
		return placentalLocation;
	}

	public void setPlacentalLocation(String placentalLocation) {
		this.placentalLocation = placentalLocation;
	}

	public String getPregnancyAge() {
		return pregnancyAge;
	}

	public void setPregnancyAge(String pregnancyAge) {
		this.pregnancyAge = pregnancyAge;
	}

	public String getFetalMovements() {
		return fetalMovements;
	}

	public void setFetalMovements(String fetalMovements) {
		this.fetalMovements = fetalMovements;
	}

	public Boolean getFetalPole() {
		return fetalPole;
	}

	public void setFetalPole(Boolean fetalPole) {
		this.fetalPole = fetalPole;
	}

	public Boolean getYolkSac() {
		return yolkSac;
	}

	public void setYolkSac(Boolean yolkSac) {
		this.yolkSac = yolkSac;
	}

	public Float getForBiometricsWeek() {
		return forBiometricsWeek;
	}

	public void setForBiometricsWeek(Float forBiometricsWeek) {
		this.forBiometricsWeek = forBiometricsWeek;
	}

	public Float getForWeek() {
		return forWeek;
	}

	public void setForWeek(Float forWeek) {
		this.forWeek = forWeek;
	}

}
